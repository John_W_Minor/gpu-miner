INLINE DEVICE void sha3_Compute(ULONG *sponge)
{
    UINT i;

    ULONG a00 = sponge[0];
    ULONG a01 = sponge[1];
    ULONG a02 = sponge[2];
    ULONG a03 = sponge[3];
    ULONG a04 = sponge[4];
    ULONG a10 = sponge[5];
    ULONG a11 = sponge[6];
    ULONG a12 = sponge[7];
    ULONG a13 = sponge[8];
    ULONG a14 = sponge[9];
    ULONG a20 = sponge[10];
    ULONG a21 = sponge[11];
    ULONG a22 = sponge[12];
    ULONG a23 = sponge[13];
    ULONG a24 = sponge[14];
    ULONG a30 = sponge[15];
    ULONG a31 = sponge[16];
    ULONG a32 = sponge[17];
    ULONG a33 = sponge[18];
    ULONG a34 = sponge[19];
    ULONG a40 = sponge[20];
    ULONG a41 = sponge[21];
    ULONG a42 = sponge[22];
    ULONG a43 = sponge[23];
    ULONG a44 = sponge[24];

    ULONG bc0;
    ULONG bc1;
    ULONG bc2;
    ULONG bc3;
    ULONG bc4;

    ULONG t;

    #pragma unroll 12
    for(i=0; i<24; i++)
    {
        // Theta

        bc0 = a00 ^ a10 ^ a20 ^ a30 ^ a40;
        bc1 = a01 ^ a11 ^ a21 ^ a31 ^ a41;
        bc2 = a02 ^ a12 ^ a22 ^ a32 ^ a42;
        bc3 = a03 ^ a13 ^ a23 ^ a33 ^ a43;
        bc4 = a04 ^ a14 ^ a24 ^ a34 ^ a44;

        t = bc4 ^ ROL64(bc1, 1); a00 ^= t; a10 ^= t; a20 ^= t; a30 ^= t; a40 ^= t;
        t = bc0 ^ ROL64(bc2, 1); a01 ^= t; a11 ^= t; a21 ^= t; a31 ^= t; a41 ^= t;
        t = bc1 ^ ROL64(bc3, 1); a02 ^= t; a12 ^= t; a22 ^= t; a32 ^= t; a42 ^= t;
        t = bc2 ^ ROL64(bc4, 1); a03 ^= t; a13 ^= t; a23 ^= t; a33 ^= t; a43 ^= t;
        t = bc3 ^ ROL64(bc0, 1); a04 ^= t; a14 ^= t; a24 ^= t; a34 ^= t; a44 ^= t;

        //rhopi

        #define Rho_Pi(ad,r)     \
          bc0 = ad;              \
          ad = ROL64(t, r);      \
          t = bc0;               \

        t = a01;

        Rho_Pi (a20,  1);
        Rho_Pi (a12,  3);
        Rho_Pi (a21,  6);
        Rho_Pi (a32, 10);
        Rho_Pi (a33, 15);
        Rho_Pi (a03, 21);
        Rho_Pi (a10, 28);
        Rho_Pi (a31, 36);
        Rho_Pi (a13, 45);
        Rho_Pi (a41, 55);
        Rho_Pi (a44,  2);
        Rho_Pi (a04, 14);
        Rho_Pi (a30, 27);
        Rho_Pi (a43, 41);
        Rho_Pi (a34, 56);
        Rho_Pi (a23,  8);
        Rho_Pi (a22, 25);
        Rho_Pi (a02, 43);
        Rho_Pi (a40, 62);
        Rho_Pi (a24, 18);
        Rho_Pi (a42, 39);
        Rho_Pi (a14, 61);
        Rho_Pi (a11, 20);
        Rho_Pi (a01, 44);

        //chi

        bc0 = a00; bc1 = a01; bc2 = a02; bc3 = a03; bc4 = a04;
        a00 ^= ~bc1 & bc2; a01 ^= ~bc2 & bc3; a02 ^= ~bc3 & bc4; a03 ^= ~bc4 & bc0; a04 ^= ~bc0 & bc1;

        bc0 = a10; bc1 = a11; bc2 = a12; bc3 = a13; bc4 = a14;
        a10 ^= ~bc1 & bc2; a11 ^= ~bc2 & bc3; a12 ^= ~bc3 & bc4; a13 ^= ~bc4 & bc0; a14 ^= ~bc0 & bc1;

        bc0 = a20; bc1 = a21; bc2 = a22; bc3 = a23; bc4 = a24;
        a20 ^= ~bc1 & bc2; a21 ^= ~bc2 & bc3; a22 ^= ~bc3 & bc4; a23 ^= ~bc4 & bc0; a24 ^= ~bc0 & bc1;

        bc0 = a30; bc1 = a31; bc2 = a32; bc3 = a33; bc4 = a34;
        a30 ^= ~bc1 & bc2; a31 ^= ~bc2 & bc3; a32 ^= ~bc3 & bc4; a33 ^= ~bc4 & bc0; a34 ^= ~bc0 & bc1;

        bc0 = a40; bc1 = a41; bc2 = a42; bc3 = a43; bc4 = a44;
        a40 ^= ~bc1 & bc2; a41 ^= ~bc2 & bc3; a42 ^= ~bc3 & bc4; a43 ^= ~bc4 & bc0; a44 ^= ~bc0 & bc1;

        //iota

        a00 ^= KeccakRoundConstants[i];
    }

    sponge[0] = a00;
    sponge[1] = a01;
    sponge[2] = a02;
    sponge[3] = a03;
    sponge[4] = a04;
    sponge[5] = a10;
    sponge[6] = a11;
    sponge[7] = a12;
    sponge[8] = a13;
    sponge[9] = a14;
    sponge[10] = a20;
    sponge[11] = a21;
    sponge[12] = a22;
    sponge[13] = a23;
    sponge[14] = a24;
    sponge[15] = a30;
    sponge[16] = a31;
    sponge[17] = a32;
    sponge[18] = a33;
    sponge[19] = a34;
    sponge[20] = a40;
    sponge[21] = a41;
    sponge[22] = a42;
    sponge[23] = a43;
    sponge[24] = a44;
}