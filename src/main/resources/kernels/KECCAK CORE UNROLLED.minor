INLINE DEVICE void sha3_Compute(ULONG *sponge)
{
    UINT i;

    ULONG bc0;
    ULONG bc1;
    ULONG bc2;
    ULONG bc3;
    ULONG bc4;

    ULONG t;

    #pragma unroll 12
    for(i=0; i<24; i++)
    {
        // Theta

        bc0 = sponge[0] ^ sponge[5] ^ sponge[10] ^ sponge[15] ^ sponge[20];
        bc1 = sponge[1] ^ sponge[6] ^ sponge[11] ^ sponge[16] ^ sponge[21];
        bc2 = sponge[2] ^ sponge[7] ^ sponge[12] ^ sponge[17] ^ sponge[22];
        bc3 = sponge[3] ^ sponge[8] ^ sponge[13] ^ sponge[18] ^ sponge[23];
        bc4 = sponge[4] ^ sponge[9] ^ sponge[14] ^ sponge[19] ^ sponge[24];

        t = bc4 ^ ROL64(bc1, 1); sponge[0] ^= t; sponge[5] ^= t; sponge[10] ^= t; sponge[15] ^= t; sponge[20] ^= t;
        t = bc0 ^ ROL64(bc2, 1); sponge[1] ^= t; sponge[6] ^= t; sponge[11] ^= t; sponge[16] ^= t; sponge[21] ^= t;
        t = bc1 ^ ROL64(bc3, 1); sponge[2] ^= t; sponge[7] ^= t; sponge[12] ^= t; sponge[17] ^= t; sponge[22] ^= t;
        t = bc2 ^ ROL64(bc4, 1); sponge[3] ^= t; sponge[8] ^= t; sponge[13] ^= t; sponge[18] ^= t; sponge[23] ^= t;
        t = bc3 ^ ROL64(bc0, 1); sponge[4] ^= t; sponge[9] ^= t; sponge[14] ^= t; sponge[19] ^= t; sponge[24] ^= t;

        //rhopi

        #define Rho_Pi(ad,r)     \
          bc0 = ad;              \
          ad = ROL64(t, r);      \
          t = bc0;               \

        t = sponge[1];

        Rho_Pi (sponge[10],  1);
        Rho_Pi (sponge[7],  3);
        Rho_Pi (sponge[11],  6);
        Rho_Pi (sponge[17], 10);
        Rho_Pi (sponge[18], 15);
        Rho_Pi (sponge[3], 21);
        Rho_Pi (sponge[5], 28);
        Rho_Pi (sponge[16], 36);
        Rho_Pi (sponge[8], 45);
        Rho_Pi (sponge[21], 55);
        Rho_Pi (sponge[24],  2);
        Rho_Pi (sponge[4], 14);
        Rho_Pi (sponge[15], 27);
        Rho_Pi (sponge[23], 41);
        Rho_Pi (sponge[19], 56);
        Rho_Pi (sponge[13],  8);
        Rho_Pi (sponge[12], 25);
        Rho_Pi (sponge[2], 43);
        Rho_Pi (sponge[20], 62);
        Rho_Pi (sponge[14], 18);
        Rho_Pi (sponge[22], 39);
        Rho_Pi (sponge[9], 61);
        Rho_Pi (sponge[6], 20);
        Rho_Pi (sponge[1], 44);

        //chi

        bc0 = sponge[0]; bc1 = sponge[1]; bc2 = sponge[2]; bc3 = sponge[3]; bc4 = sponge[4];
        sponge[0] ^= ~bc1 & bc2; sponge[1] ^= ~bc2 & bc3; sponge[2] ^= ~bc3 & bc4; sponge[3] ^= ~bc4 & bc0; sponge[4] ^= ~bc0 & bc1;

        bc0 = sponge[5]; bc1 = sponge[6]; bc2 = sponge[7]; bc3 = sponge[8]; bc4 = sponge[9];
        sponge[5] ^= ~bc1 & bc2; sponge[6] ^= ~bc2 & bc3; sponge[7] ^= ~bc3 & bc4; sponge[8] ^= ~bc4 & bc0; sponge[9] ^= ~bc0 & bc1;

        bc0 = sponge[10]; bc1 = sponge[11]; bc2 = sponge[12]; bc3 = sponge[13]; bc4 = sponge[14];
        sponge[10] ^= ~bc1 & bc2; sponge[11] ^= ~bc2 & bc3; sponge[12] ^= ~bc3 & bc4; sponge[13] ^= ~bc4 & bc0; sponge[14] ^= ~bc0 & bc1;

        bc0 = sponge[15]; bc1 = sponge[16]; bc2 = sponge[17]; bc3 = sponge[18]; bc4 = sponge[19];
        sponge[15] ^= ~bc1 & bc2; sponge[16] ^= ~bc2 & bc3; sponge[17] ^= ~bc3 & bc4; sponge[18] ^= ~bc4 & bc0; sponge[19] ^= ~bc0 & bc1;

        bc0 = sponge[20]; bc1 = sponge[21]; bc2 = sponge[22]; bc3 = sponge[23]; bc4 = sponge[24];
        sponge[20] ^= ~bc1 & bc2; sponge[21] ^= ~bc2 & bc3; sponge[22] ^= ~bc3 & bc4; sponge[23] ^= ~bc4 & bc0; sponge[24] ^= ~bc0 & bc1;

        //iota

        sponge[0] ^= KeccakRoundConstants[i];
    }
}