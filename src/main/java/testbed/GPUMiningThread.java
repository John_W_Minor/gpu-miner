package testbed;

import gpuminer.GPULogging;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.databuckets.BlockAndSharePayloads;
import gpuminer.miner.databuckets.Payload;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.SHA3.SHA3Miner;
import gpuminer.miner.synchron.Synchron;
import logging.IAmpexLogger;
import org.bouncycastle.jcajce.provider.digest.SHA3;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.util.Random;

/**
 * Created by Queue on 10/12/2017.
 */
public class GPUMiningThread implements Runnable
{
    private SHA3Miner miner;
    private DeviceContext context;

    private byte[] blockDifficulty;
    private byte[] shareDifficulty = null;
    private int threadFactor;

    private String deviceName;
    private String vendor;

    private Thread miningThread;

    private KernelType kernelType;

    private int blockCount = 0;
    private int shareCount = 0;
    private boolean greedy;

    public GPUMiningThread(DeviceContext _context, byte[] _blockDifficulty, byte[] _shareDifficulty, int _threadFactor, KernelType _kernelType, boolean _greedy, int _inten)
    {
        inten = (double) _inten / 100;

        if (_inten == 100 || _inten > 100)
        {
            inten = 1;
        }

        if (_inten < 10)
        {
            inten = .1;
        }


        context = _context;
        blockDifficulty = _blockDifficulty.clone();
        if (_shareDifficulty != null)
        {
            shareDifficulty = _shareDifficulty.clone();
        }

        threadFactor = _threadFactor;

        kernelType = _kernelType;

        miner = new SHA3Miner(context, blockDifficulty, shareDifficulty, threadFactor,kernelType, _greedy);

        greedy = _greedy;

        deviceName = context.getDInfo().getDeviceName();
        vendor = context.getDInfo().getVendor();
        miningThread = new Thread(this, "GPUMiningThread " + deviceName);
        miningThread.start();
    }

    public void run()
    {
        while (true)
        {
            mine();
        }
    }

    private Random rand = new Random();

    private double inten;

    public void mine()
    {
        IAmpexLogger logger = GPULogging.getLogger();

        //byte[] message = new byte[new Random().nextInt(100000) + 5];
        byte[] message = new byte[new Random().nextInt(2000) + 1];
        rand.nextBytes(message);

        byte[] entropy = new byte[71];
        for (int i = 0; i < entropy.length; i++)
        {
            entropy[i] = "a".getBytes(Constants.UTF8)[0];
        }

        entropy = (inten + " intensity with no oscillation and " + greedy + " greed. ").getBytes(Constants.UTF8);

        miner.setIntensity(inten);

        //greedy = !greedy;

        //miner.recompileKernel(blockDifficulty, shareDifficulty, greedy);

        miner.resumeMining(message, entropy);

        //miner.setIntensity(inten);

        if (logger != null)
        {
            System.out.println();
            logger.debug("Mining intensity set to " + inten + ".");
        }

        if (logger != null)
        {
            System.out.println();
            logger.debug("Mining started on device: " + vendor + " " + deviceName + " with " + kernelType.name() + " kernel.");
        }

        double intensity = 1;

        while (miner.isMining().get())
        {
            Synchron.idle();
        }

        if (logger != null)
        {
            logger.debug("Mining stopped on device: " + vendor + " " + deviceName + " with " + kernelType.name() + " kernel.");
            logger.debug("Hashrate was: " + NumberFormat.getNumberInstance().format(miner.getHashesPerSecond()));
        }

        BlockAndSharePayloads[] payloads = miner.getPayloads();

        if (payloads != null)
        {
            for (int i = 0; i < payloads.length; i++)
            {
                if (payloads[i].wasBlockFound())
                {
                    blockCount++;

                    if (logger != null)
                    {
                        System.out.println();
                        logger.debug("Solution found on device: " + vendor + " " + deviceName + " with " + kernelType.name() + " kernel.");
                    }

                    byte[] winningPayload = payloads[i].getBlockPayload().getBytes();
                    byte[] winningMessage = ByteBuffer.allocate(message.length + winningPayload.length).put(message).put(winningPayload).array();

                    byte[] winningHash = sha512(winningMessage);

                    BigInteger currentDifficulty = new BigInteger(blockDifficulty).abs();

                    byte[] byteDiff = new byte[64];
                    int p = 63;
                    for (int k = currentDifficulty.toByteArray().length - 1; k >= 0; k--)
                    {
                        byteDiff[p] = currentDifficulty.toByteArray()[k];
                        p--;
                    }

                    BigInteger bigIntHash = new BigInteger(winningHash).abs();
                    byte[] byteHash = new byte[64];
                    p = 63;
                    for (int k = bigIntHash.toByteArray().length - 1; k >= 0; k--)
                    {
                        byteHash[p] = bigIntHash.toByteArray()[k];
                        p--;
                    }

                    int mostSignificant0Digits = 0;
                    for (int k = 0; k < byteDiff.length; k++)
                    {
                        mostSignificant0Digits = k;
                        if (byteDiff[k] != 0)
                        {
                            break;
                        }
                    }
                    int mostSignificantByte = byteDiff[mostSignificant0Digits] & 0x0ff;

                    int precedingZeroes = 0;
                    for (int k = 0; k < mostSignificant0Digits; k++)
                    {
                        precedingZeroes = (precedingZeroes | byteHash[k]) & 0x00ff;
                    }

                    if (!(precedingZeroes == 0 && byteHash[mostSignificant0Digits] < mostSignificantByte))
                    {
                        if (logger != null)
                        {
                            logger.debug("error detected no solver");
                        }
                    }

                    if (logger != null)
                    {
                        logger.debug("Device: " + vendor + " " + deviceName + " DIFF: " + HexHelper.toHexArray(blockDifficulty));
                        logger.debug("Device: " + vendor + " " + deviceName + " HASH: " + HexHelper.toHexArray(winningHash));

                        logger.debug("Device: " + vendor + " " + deviceName + " LOAD: " + new String(winningPayload, Constants.UTF8));
                        logger.debug("Device: " + vendor + " " + deviceName + " SIZE: " + winningPayload.length);

                        if (miner.getThreads() != -1)
                            logger.debug("Device: " + vendor + " " + deviceName + " THRD: " + NumberFormat.getNumberInstance().format(miner.getThreads()));


                        if (miner.getHashesPerSecond().get() != -1)
                        {
                            logger.debug("Hashrate on device: " + vendor + " " + deviceName + " is: " + NumberFormat.getNumberInstance().format(miner.getHashesPerSecond()) + " hashes per second.");
                        }
                    }
                }

                if (payloads[i].wasShareFound())
                {
                    Payload[] shares = payloads[i].getSharePayloads();
                    if (logger != null)
                    {
                        System.out.println();
                        logger.debug(shares.length + " of " + miner.getKInfo().getSharesSpace() + " shares found:");
                        logger.debug("Device: " + vendor + " " + deviceName + " DIFF: " + HexHelper.toHexArray(shareDifficulty));
                    }
                    for (int j = 0; j < shares.length; j++)
                    {
                        shareCount++;
                        byte[] sharePayload = shares[j].getBytes();

                        byte[] shareMessage = ByteBuffer.allocate(message.length + sharePayload.length).put(message).put(sharePayload).array();

                        byte[] shareHash = sha512(shareMessage);

                        if (logger != null)
                        {
                            logger.debug("Device: " + vendor + " " + deviceName + " HASH: " + HexHelper.toHexArray(shareHash));
                        }
                    }

                    if (miner.getHashesPerSecond().get() != -1)
                    {
                        if (logger != null)
                        {
                            System.out.println();
                            logger.debug("Hashrate on device: " + vendor + " " + deviceName + " is: " + NumberFormat.getNumberInstance().format(miner.getHashesPerSecond()) + " hashes per second.");
                        }
                    }
                }
            }
        }

        if (logger != null)
        {
            System.out.println();
            logger.debug("Found " + blockCount + " blocks.");
            logger.debug("Found " + shareCount + " shares.");
        }
    }

    private static byte[] sha512(byte[] input)
    {
        SHA3.DigestSHA3 md = new SHA3.Digest512();
        md.update(input);
        return md.digest();
    }
}
