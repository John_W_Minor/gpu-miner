package testbed;

import gpuminer.GPULogging;
import gpuminer.miner.autotune.TimedAutotune;
import gpuminer.miner.context.ContextMaster;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.synchron.Synchron;

import java.util.*;

/**
 * © 2017 Ampex Technologies LLC. All Rights Reserved.
 * All other files included in this library are similarly copyrighted.
 */
public class Main
{
    //TODO see what can be done about the atomic exchange stuff in the mining kernels.

    static byte[] blockDifficulty = new byte[64];
    static byte[] shareDifficulty = null;
    static boolean forceAutotune = false;
    static boolean greedy = false;
    static int inten = 100;

    public static void main(String[] args) throws Exception
    {
        //ContextMaster.disableOCL();

        GPULogging.startLogging();

        String[] fargs = {"-bd", "8", "-100" , "-a", "-ng"};

        readArgs(fargs);

        readArgs(args);

        ContextMaster platforms = new ContextMaster();

        ArrayList<DeviceContext> contexts = platforms.getContexts();

        TimedAutotune.setup(contexts, forceAutotune);


        for (DeviceContext context : contexts)
        {

            if (TimedAutotune.getAutotuneSettingsMap().containsKey(context.getDInfo().getDeviceName()))
            {
                new GPUMiningThread(context, blockDifficulty, shareDifficulty, TimedAutotune.getAutotuneSettingsMap().get(context.getDInfo().getDeviceName()).threadFactor, TimedAutotune.getAutotuneSettingsMap().get(context.getDInfo().getDeviceName()).kernelType, greedy, inten);
            } else
            {
                new GPUMiningThread(context, blockDifficulty, shareDifficulty, 5, KernelType.OCL_MAIN, false, 50);
            }
        }
    }

    private static boolean autotune = false;

    public static void readArgs(String[] args)
    {
        int index = 0;
        for (String arg : args)
        {
            index++;

            try
            {
                inten = Integer.parseInt(arg.substring(1));
            }catch (Exception e)
            {
            }

            if (arg.equals("-g"))
            {
                greedy = true;
            } else if (arg.equals("-ng"))
            {
                greedy = false;
            } else if(arg.equals("-bd"))
            {

                //new Random().nextBytes(blockDifficulty);

                int diff = Integer.parseInt(args[index]);

                int diffHalf = diff / 2;

                int diffMod = diff % 2;

                for (int i = 0; i < diffHalf; i++)
                {
                    blockDifficulty[i] = 0;
                }

                if (diffMod == 1)
                {
                    blockDifficulty[diffHalf] = 0x0f;
                } else
                {
                    blockDifficulty[diffHalf] = -1;
                }

                //blockDifficulty = new byte[64];
            } else if (arg.equals("-sd"))
            {
                shareDifficulty = new byte[64];
                //new Random().nextBytes(shareDifficulty);

                int diff = Integer.parseInt(args[index]);

                int diffHalf = diff / 2;

                int diffMod = diff % 2;

                for (int i = 0; i < diffHalf; i++)
                {
                    shareDifficulty[i] = 0;
                }

                if (diffMod == 1)
                {
                    shareDifficulty[diffHalf] = 0x0f;
                } else
                {
                    shareDifficulty[diffHalf] = -1;
                }

            } else if (arg.equals("-fa"))
            {
                forceAutotune = true;
            }

        }
    }

}
