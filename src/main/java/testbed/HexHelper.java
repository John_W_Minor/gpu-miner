package testbed;

/**
 * Created by Queue on 10/3/2017.
 */
public class HexHelper
{
    public static String toHexArray(byte[] array)
    {
        StringBuilder sb = new StringBuilder();
        for (byte b : array)
        {
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }
}
