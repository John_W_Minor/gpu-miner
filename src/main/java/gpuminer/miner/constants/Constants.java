package gpuminer.miner.constants;

import java.nio.charset.Charset;

import static org.jocl.CL.CL_DEVICE_TYPE_ALL;

/**
 * Created by Queue on 11/7/2017.
 */
public class Constants
{
    //device types
    public final static String TYPE_GPU = "CL_DEVICE_TYPE_GPU";
    public final static String TYPE_CPU = "CL_DEVICE_TYPE_CPU";
    public final static String TYPE_ACCELERATOR = "CL_DEVICE_TYPE_ACCELERATOR";
    public final static String TYPE_DEFAULT = "CL_DEVICE_TYPE_DEFAULT";

    //supported device vendors
    private final static String[] VENDOR_AMD = {"Advanced Micro Devices, Inc.", "AuthenticAMD", "AMD"};

    private final static String[] VENDOR_NVIDIA = {"NVIDIA Corporation", "NVIDIA"};

    public static String[] getVendorAmd()
    {
        return VENDOR_AMD.clone();
    }

    public static String[] getVendorNvidia()
    {
        return VENDOR_NVIDIA.clone();
    }

    //extensions
    public final static String AMD_MEDIA_OPS = "cl_amd_media_ops";

    //kernel types
    public final static int OCL_KERNEL = 0;
//    public final static int CUDA_KERNEL = 1;

    //utf-8
    public static final Charset UTF8 = Charset.availableCharsets().get("UTF-8");

    //a wavefront is two warps, so a multiple of a wavefront is by definition a multiple of a warp
    public static final int WAVEFRONT = 64;
}
