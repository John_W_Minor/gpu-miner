package gpuminer.miner.ops;

import gpuminer.miner.infocontainers.KernelInfo;
import jcuda.driver.CUfunction;

/**
 * Created by Queue on 1/14/2018.
 */
public interface IAutotuneOp
{
    long time() throws Exception;

    KernelInfo getKInfo();

    void shutdown() throws Exception;
}
