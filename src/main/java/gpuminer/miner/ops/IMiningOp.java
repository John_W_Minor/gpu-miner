package gpuminer.miner.ops;

import com.ampex.amperabase.data.BaseValueWatchable;
import gpuminer.miner.databuckets.GPUOutput;
import gpuminer.miner.infocontainers.KernelInfo;

/**
 * Created by Queue on 11/30/2017.
 */
public interface IMiningOp
{
    GPUOutput mine(int _threads, byte[] _sponge, int _payload, int _computeUnits) throws Exception;

    BaseValueWatchable<Long> getKernelTime();

    void resetHeat();

    KernelInfo getKInfo();

    void shutdown() throws Exception;

    void setGreedy(boolean val);
}
