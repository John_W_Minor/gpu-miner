package gpuminer.miner.ops;

import com.ampex.amperabase.data.BaseValueWatchable;
import gpuminer.JOCL.ops.JOCLSHA3MiningOp;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.databuckets.GPUOutput;
import gpuminer.miner.infocontainers.KernelInfo;
import gpuminer.miner.kernel.KernelType;

/**
 * Created by Queue on 11/17/2017.
 */
public class MiningOp implements IMiningOp {

    private IMiningOp miningOp = null;

    public MiningOp(DeviceContext _context, byte[] _blockDifficulty, byte[] _shareDifficulty, int _threads, KernelType _kernelType, boolean _greedy) throws Exception {
        miningOp = new JOCLSHA3MiningOp(_context.getOCLContext(), _context.getDInfo(), _blockDifficulty, _shareDifficulty, _threads, _kernelType, _greedy);

    }

    public GPUOutput mine(int _threads, byte[] _sponge, int _payload, int _computeUnits) throws Exception {
        return miningOp.mine(_threads, _sponge, _payload, _computeUnits);
    }

    public BaseValueWatchable<Long> getKernelTime() {
        return miningOp.getKernelTime();
    }

    public void resetHeat() {
        miningOp.resetHeat();
    }

    public KernelInfo getKInfo() {
        return miningOp.getKInfo();
    }

    public void shutdown() throws Exception {
        if (miningOp != null) {
            miningOp.shutdown();
        }
    }

    @Override
    public void setGreedy(boolean val) {
        miningOp.setGreedy(val);
    }
}
