package gpuminer.miner.synchron;

import gpuminer.GPULogging;
import logging.IAmpexLogger;

/**
 * Created by Queue on 12/10/2017.
 */
public class Synchron
{
    //sleep time
    private final static int SLEEP_INTERVAL_MILLI = 0;
    private final static int SLEEP_INTERVAL_NANO = 50000;

    public static void idle()
    {
        try
        {
            //It is critical that anything that has reason to interact with an SHA3MiningThread object through an SHA3Miner object
            //be able to do what it's trying to do without worrying about synchronization locks fucking up or long delays.
            //Maybe locking objects work just fine for this purpose, but the documentation is not clear, and this hacky shit works just as well.
            //Note that all commands to SHA3MiningThread objects are filtered through SHA3Miner objects so only one command can go through at a time.
            //I was fine just using empty while loops, and this lowers the amount of cycles dedicated to nothing without sacrificing much response time.
            Thread.sleep(SLEEP_INTERVAL_MILLI, SLEEP_INTERVAL_NANO);
        }catch (Exception e)
        {
            IAmpexLogger logger = GPULogging.getLogger();
            if(logger != null)
            {
                logger.error("", e);
            }
        }
    }

    public static void idle(int _sleepMilli, int _sleepNano)
    {
        try
        {
            Thread.sleep(_sleepMilli, _sleepNano);
        }catch (Exception e)
        {
            //Logger logger = GPULogging.getLogger();
            //if(logger != null)
            {
                //logger.error("", e);
            }
        }
    }
}
