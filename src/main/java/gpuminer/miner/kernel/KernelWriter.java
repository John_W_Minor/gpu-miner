package gpuminer.miner.kernel;

import gpuminer.miner.constants.Constants;
import gpuminer.miner.infocontainers.DeviceInfo;

import java.io.*;
import java.nio.file.Paths;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Created by Queue on 11/18/2017.
 */
public class KernelWriter
{

    public static void appendSource(StringBuilder _source, String[] _filePaths) throws Exception
    {
        for (int i = 0; i < _filePaths.length; i++)
        {
            _source.append(readKernel(_filePaths[i])).append("\n");
        }
    }

    private static String readKernel(String _filePath) throws Exception
    {
        BufferedReader reader = null;

        JarFile file = null;

        FileInputStream fileInputStream = null;

        InputStreamReader inputStreamReader = null;

        try
        {

            if (KernelWriter.class.getResource("KernelWriter.class").toString().substring(0, 9).equalsIgnoreCase("jar:file:"))
            {

                File jar = Paths.get(KernelWriter.class.getProtectionDomain().getCodeSource().getLocation().toURI()).toFile();

                file = new JarFile(jar);
                JarEntry entry = new JarEntry(_filePath);

                inputStreamReader = new InputStreamReader(file.getInputStream(entry), Constants.UTF8);

                reader = new BufferedReader(inputStreamReader);
            } else
            {
                fileInputStream = new FileInputStream("src/main/resources/" + _filePath);
                inputStreamReader = new InputStreamReader(fileInputStream, Constants.UTF8);
                reader = new BufferedReader(inputStreamReader);
            }
            StringBuffer buffer = new StringBuffer();
            String line = null;
            while (true)
            {
                line = reader.readLine();
                if (line == null)
                {
                    break;
                }
                buffer.append(line).append("\n");
            }

            if (file != null)
            {
                file.close();
            }

            //if(inputStreamReader != null)
            {
                inputStreamReader.close();;
            }

            if(fileInputStream != null)
            {
                fileInputStream.close();
            }

            //if(reader != null)
            {
                reader.close();
            }

            return buffer.toString();
        } catch (Exception e)
        {
            throw e;
        } finally
        {
            if (file != null)
            {
                file.close();
            }

            if(inputStreamReader != null)
            {
                inputStreamReader.close();;
            }

            if(fileInputStream != null)
            {
                fileInputStream.close();
            }

            if(reader != null)
            {
                reader.close();
            }
        }
    }

    public static void appendBlockDifficulty(StringBuilder _source, byte[] _blockDifficulty)
    {
        if (_blockDifficulty != null)
        {
            int mostSignificant0Digits = calculateMostSignificant0Digits(_blockDifficulty);

            int mostSignificantByte = calculateMostSignificantByte(mostSignificant0Digits, _blockDifficulty);

            _source.append("#define BLOCK_DIFFICULTY_LENGTH ").append(mostSignificant0Digits).append("\n");
            _source.append("#define BLOCK_MOST_SIGNIFICANT_BYTE ").append(mostSignificantByte).append("\n");
        }
    }

    public static void appendShareDifficulty(StringBuilder _source, byte[] _shareDifficulty)
    {
        if (_shareDifficulty != null)
        {
            int mostSignificant0Digits = calculateMostSignificant0Digits(_shareDifficulty);

            int mostSignificantByte = calculateMostSignificantByte(mostSignificant0Digits, _shareDifficulty);

            _source.append("#define ENABLE_SHARES\n");
            _source.append("#define SHARE_DIFFICULTY_LENGTH ").append(mostSignificant0Digits).append("\n");
            _source.append("#define SHARE_MOST_SIGNIFICANT_BYTE ").append(mostSignificantByte).append("\n");
        }
    }

    public static int appendSharesSpace(StringBuilder _source, byte[] _shareDifficulty, int _threads)
    {
        int sharesSpace = 1;

        if (_shareDifficulty != null)
        {
            if (_threads != 0)
            {
                int mostSignificant0Digits = calculateMostSignificant0Digits(_shareDifficulty);

                int mostSignificantByte = calculateMostSignificantByte(mostSignificant0Digits, _shareDifficulty);

                int zeroes = 2 * mostSignificant0Digits;

                if (mostSignificantByte <= 15)
                {
                    zeroes++;
                }

                sharesSpace = (int) ((1 / Math.pow(16, zeroes)) * _threads * 2) + 10;

                int limit = 10000;

                if (sharesSpace > limit)
                {
                    sharesSpace = limit;
                }
            }

            _source.append("#define SHARESSPACE ").append(sharesSpace);
        }

        return sharesSpace;
    }

    private static int calculateMostSignificant0Digits(byte[] _difficulty)
    {
        int mostSignificant0Digits = 0;

        for (int i = 0; i < _difficulty.length; i++)
        {
            mostSignificant0Digits = i;
            if (_difficulty[i] != 0)
            {
                break;
            }
        }

        return mostSignificant0Digits;
    }

    private static int calculateMostSignificantByte(int _mostSignificant0Digits, byte[] _difficulty)
    {
        return _difficulty[_mostSignificant0Digits] & 0x0ff;
    }

    public static void appendAMD_MEDIA_OPSIfApplicable(StringBuilder _source, DeviceInfo _dInfo)
    {
        String type = _dInfo.getDeviceType();

        String[] extensions = _dInfo.getExtensions();

        boolean amdMediaOpsCompatible = false;

        for (int i = 0; i < extensions.length; i++)
        {
            if (extensions[i].equals(Constants.AMD_MEDIA_OPS))
            {
                amdMediaOpsCompatible = true;
            }
        }

        if (type.equals(Constants.TYPE_GPU) && amdMediaOpsCompatible)
        {
            _source.append("#define AMD_MEDIA_OPS\n");
        }
    }
}
