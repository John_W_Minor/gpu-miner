package gpuminer.miner.kernel;

import static gpuminer.miner.constants.Constants.OCL_KERNEL;

public enum KernelType {

    OCL_MAIN("kernels/KECCAK CORE.minor"),
    OCL_MAIN_UNROLLED("kernels/KECCAK CORE UNROLLED.minor"),
    OCL_MAIN_MORE_MEMORY("kernels/KECCAK CORE MORE MEMORY.minor"),
    OCL_MAIN_MORE_MEMORY_UNROLLED("kernels/KECCAK CORE MORE MEMORY UNROLLED.minor"),

//    @Deprecated
//    CUDA_MAIN("kernels/KECCAK CORE.minor", Constants.CUDA_KERNEL),
//    @Deprecated
//    CUDA_MAIN_UNROLLED("kernels/KECCAK CORE UNROLLED.minor", Constants.CUDA_KERNEL),
//    @Deprecated
//    CUDA_MAIN_MORE_MEMORY("kernels/KECCAK CORE MORE MEMORY.minor", Constants.CUDA_KERNEL),
//    @Deprecated
//    CUDA_MAIN_MORE_MEMORY_UNROLLED("kernels/KECCAK CORE MORE MEMORY UNROLLED.minor", Constants.CUDA_KERNEL)
    ;

    private final String source;

    KernelType(String _source)
    {
        source = _source;
    }

    public String getSource()
    {
        return source;
    }

    public int getType()
    {
        return OCL_KERNEL;
    }
}
