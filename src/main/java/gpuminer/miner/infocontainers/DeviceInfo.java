package gpuminer.miner.infocontainers;

import gpuminer.GPULogging;
import gpuminer.miner.constants.Constants;
import jcuda.driver.CUdevice;
import logging.IAmpexLogger;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_device_id;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static jcuda.driver.CUdevice_attribute.*;
import static jcuda.driver.JCudaDriver.cuDeviceGetAttribute;
import static jcuda.driver.JCudaDriver.cuDeviceGetName;
import static jcuda.driver.JCudaDriver.cuDriverGetVersion;
import static org.jocl.CL.*;

/**
 * Created by Queue on 10/11/2017.
 */
public class DeviceInfo
{
    private cl_device_id oclDevice = null;

    private CUdevice cudaDevice = null;

    private String deviceName = null;

    private String oclVersion = null;

    private int cudaVersion = -1;

    private String deviceType = null;

    private String vendor = null;

    private String[] extensions = null;

    private boolean availability =  false;

    private boolean littleEndian = false;

    private long maxWorkGroupSize = 0;

    private long localMemorySize = 0;

    private int maxComputeUnits = 0;

    private int warpSize = 0;

    private boolean amd =false;

    private boolean nvidia =false;

    public DeviceInfo(cl_device_id _oclDevice) throws Exception
    {
        oclDevice = _oclDevice;

        //Device Name
        byte[] deviceNameArray = new byte[1024];
        clGetDeviceInfo(oclDevice, CL_DEVICE_NAME, 100 * Sizeof.cl_char, Pointer.to(deviceNameArray), null);
        deviceName = new String(deviceNameArray, Constants.UTF8).trim();

        //OpenCL version
        byte[] openCLVersionArray = new byte[1024];
        clGetDeviceInfo(oclDevice, CL_DEVICE_VERSION, 100 * Sizeof.cl_char, Pointer.to(openCLVersionArray), null);
        oclVersion = new String(openCLVersionArray, Constants.UTF8).trim();

        long[] deviceTypeArray = new long[1];
        clGetDeviceInfo(oclDevice, CL_DEVICE_TYPE, Sizeof.cl_long, Pointer.to(deviceTypeArray), null);
        if ((deviceTypeArray[0] & CL_DEVICE_TYPE_CPU) != 0)
        {
            deviceType = Constants.TYPE_CPU;
        } else if ((deviceTypeArray[0] & CL_DEVICE_TYPE_GPU) != 0)
        {
            deviceType = Constants.TYPE_GPU;
        } else if ((deviceTypeArray[0] & CL_DEVICE_TYPE_ACCELERATOR) != 0)
        {
            deviceType = Constants.TYPE_ACCELERATOR;
        } else if ((deviceTypeArray[0] & CL_DEVICE_TYPE_DEFAULT) != 0)
        {
            deviceType = Constants.TYPE_DEFAULT;
        }

        //OpenCL version
        byte[] vendorArray = new byte[1024];
        clGetDeviceInfo(oclDevice, CL_DEVICE_VENDOR, 100 * Sizeof.cl_char, Pointer.to(vendorArray), null);
        vendor = new String(vendorArray, Constants.UTF8).trim();

        byte[] extensionsArray = new byte[8192];
        clGetDeviceInfo(oclDevice, CL_DEVICE_EXTENSIONS, 8192 * Sizeof.cl_char, Pointer.to(extensionsArray), null);
        extensions = new String(extensionsArray, Constants.UTF8).trim().split(" ");

        //Availability
        int[] availabilityArray = new int[1];
        clGetDeviceInfo(oclDevice, CL_DEVICE_AVAILABLE, Sizeof.cl_int, Pointer.to(availabilityArray), null);
        availability = availabilityArray[0] == 1;

        //Endianness
        int[] littleEndianArray = new int[1];
        clGetDeviceInfo(oclDevice, CL_DEVICE_ENDIAN_LITTLE, Sizeof.cl_int, Pointer.to(littleEndianArray), null);
        littleEndian = littleEndianArray[0] == 1;

        //Max work group size
        byte[] maxWorkGroupSizeByteArray = new byte[Sizeof.size_t];
        clGetDeviceInfo(oclDevice, CL_DEVICE_MAX_WORK_GROUP_SIZE, Sizeof.size_t, Pointer.to(maxWorkGroupSizeByteArray), null);

        ByteBuffer maxWorkGroupSizeBuffer = ByteBuffer.allocate(Sizeof.size_t);
        if(littleEndian)
        {
            maxWorkGroupSizeBuffer.order(ByteOrder.LITTLE_ENDIAN);
        } else
        {
            maxWorkGroupSizeBuffer.order(ByteOrder.BIG_ENDIAN);
        }
        maxWorkGroupSizeBuffer.put(maxWorkGroupSizeByteArray);

        if (Sizeof.size_t == 4)
        {
            maxWorkGroupSize = maxWorkGroupSizeBuffer.getInt(0);
        } else
        {
            maxWorkGroupSize = maxWorkGroupSizeBuffer.getLong(0);
        }

        long[] localMemorySizeArray = new long[1];
        clGetDeviceInfo(oclDevice, CL_DEVICE_LOCAL_MEM_SIZE, Sizeof.cl_long, Pointer.to(localMemorySizeArray), null);
        localMemorySize = localMemorySizeArray[0];

        int[] maxComputeUnitsArray = new int[1];
        clGetDeviceInfo(oclDevice, CL_DEVICE_MAX_COMPUTE_UNITS, Sizeof.cl_uint, Pointer.to(maxComputeUnitsArray), null);
        maxComputeUnits = maxComputeUnitsArray[0];

        for(String vendorName:Constants.getVendorAmd())
        {
            if(vendorName.equals(vendor))
            {
                amd =true;
                break;
            }
        }

        for(String vendorName:Constants.getVendorNvidia())
        {
            if(vendorName.equals(vendor))
            {
                nvidia =true;
                break;
            }
        }

        printDeviceInfo();
    }

    @Deprecated
    public DeviceInfo(CUdevice _cudaDevice) throws Exception
    {
        cudaDevice = _cudaDevice;

        byte deviceNameArray[] = new byte[1024];
        cuDeviceGetName(deviceNameArray, deviceNameArray.length, cudaDevice);
        deviceName = new String(deviceNameArray, Constants.UTF8).trim();

        int[] cudaVersionArray = new int[1];
        cuDriverGetVersion(cudaVersionArray);
        cudaVersion = cudaVersionArray[0];

        deviceType = Constants.TYPE_GPU;

        vendor = Constants.getVendorNvidia()[0];

        availability = true;

        littleEndian = true;

        int[] maxWorkGroupSizeArray = new int[1];
        cuDeviceGetAttribute(maxWorkGroupSizeArray, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_BLOCK, cudaDevice);
        maxWorkGroupSize = maxWorkGroupSizeArray[0];

        int[] localMemorySizeArray = new int[1];
        cuDeviceGetAttribute(localMemorySizeArray, CU_DEVICE_ATTRIBUTE_MAX_SHARED_MEMORY_PER_BLOCK, cudaDevice);
        localMemorySize = localMemorySizeArray[0];

        int[] maxComputeUnitsArray = new int[1];
        cuDeviceGetAttribute(maxComputeUnitsArray, CU_DEVICE_ATTRIBUTE_MULTIPROCESSOR_COUNT, cudaDevice);
        maxComputeUnits = maxComputeUnitsArray[0];

        int[] maxThreadsPerComputeUnitArray = new int[1];
        cuDeviceGetAttribute(maxThreadsPerComputeUnitArray, CU_DEVICE_ATTRIBUTE_MAX_THREADS_PER_MULTIPROCESSOR, cudaDevice);
        int maxThreadPerComputeUnit = maxThreadsPerComputeUnitArray[0];

        int blocksPerComputeUnit = (int)(maxThreadPerComputeUnit/maxWorkGroupSize);
        //maxComputeUnits*=blocksPerComputeUnit;

        int[] warpSizeArray = new int[1];
        cuDeviceGetAttribute(warpSizeArray, CU_DEVICE_ATTRIBUTE_WARP_SIZE, cudaDevice);
        warpSize = warpSizeArray[0];

        nvidia =true;

        printDeviceInfo();
    }

    private void printDeviceInfo()
    {
        IAmpexLogger logger = GPULogging.getLogger();

        if(logger != null)
        {
            System.out.println();
            logger.info(deviceName + " found.");
        }

        if(deviceType.equals(Constants.TYPE_GPU))
        {
            if(logger != null)
            {
                logger.info(deviceName + " is a GPU.");
            }
        }else
        {
            if(logger != null)
            {
                logger.info(deviceName + " is not a GPU.");
            }
        }

        if(availability)
        {
            if(logger != null)
            {
                logger.info(deviceName + " is available.");
            }
        }

        if(amd)
        {
            //System.out.println(deviceName + " is an AMD device.");
        }

        if(nvidia)
        {
            //System.out.println(deviceName + " is an Nvidia device.");
        }

        if(!amd && !nvidia)
        {
            //System.out.println(deviceName + " is not an AMD or Nvidia device.");
        }

        if(deviceType.equals(Constants.TYPE_GPU) && availability)// && (amd || nvidia))
        {
            if(logger != null)
            {
                logger.info(deviceName + " is a compatible device.");
            }
        }else
        {
            if(logger != null)
            {
                logger.info(deviceName + " is not a compatible device.");
            }
        }
    }


    public cl_device_id getOCLDevice()
    {
        return oclDevice;
    }

    public CUdevice getCUDADevice()
    {
        return cudaDevice;
    }

    public String getDeviceName()
    {
        return deviceName;
    }

    public String getOCLVersion()
    {
        return oclVersion;
    }

    public int getCUDAVersion()
    {
        return cudaVersion;
    }

    public String getDeviceType()
    {
        return deviceType;
    }

    public String getVendor()
    {
        return vendor;
    }

    public String[] getExtensions()
    {
        if (extensions != null)
        {
            return extensions.clone();
        }
        return null;
    }

    public boolean isAvailable()
    {
        return availability;
    }

    public boolean isLittleEndian()
    {
        return littleEndian;
    }

    public long getMaxWorkGroupSize()
    {
        return maxWorkGroupSize;
    }

    public long getLocalMemorySize()
    {
        return localMemorySize;
    }

    public int getMaxComputeUnits()
    {
        return maxComputeUnits;
    }

    public int getWarpSize()
    {
        return warpSize;
    }

    public boolean isAmd()
    {
        return amd;
    }

    public boolean isNvidia()
    {
        return nvidia;
    }
}
