package gpuminer.miner.infocontainers;

import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_device_id;
import org.jocl.cl_kernel;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static jcuda.driver.CUfunction_attribute.CU_FUNC_ATTRIBUTE_MAX_THREADS_PER_BLOCK;
import static jcuda.driver.JCudaDriver.*;
import static org.jocl.CL.*;

/**
 * Created by Queue on 12/4/2017.
 */
public class KernelInfo
{
    private cl_kernel oclKernel;

    private String kernelPTXFileName;
    private String kernelName;

    private long kernelMaxWorkGroupSize;
    private long kernelWorkMult;
    private int sharesSpace;

    public KernelInfo(cl_kernel _kernel, cl_device_id _device, int _sharesSpace) throws Exception
    {
        oclKernel =_kernel;

        ByteBuffer kernelMaxWorkGroupSizeBuffer = ByteBuffer.allocate(Sizeof.size_t).order(ByteOrder.nativeOrder());
        clGetKernelWorkGroupInfo(oclKernel,_device,CL_KERNEL_WORK_GROUP_SIZE, Sizeof.size_t, Pointer.to(kernelMaxWorkGroupSizeBuffer),null);

        ByteBuffer kernelWorkMultBuffer = ByteBuffer.allocate(Sizeof.size_t).order(ByteOrder.nativeOrder());
        clGetKernelWorkGroupInfo(oclKernel,_device,CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE,Sizeof.size_t,Pointer.to(kernelWorkMultBuffer),null);

        if(Sizeof.size_t==4)
        {
            kernelMaxWorkGroupSize = kernelMaxWorkGroupSizeBuffer.getInt(0);
            kernelWorkMult = kernelWorkMultBuffer.getInt(0);
        } else
        {
            kernelMaxWorkGroupSize = kernelMaxWorkGroupSizeBuffer.getLong(0);
            kernelWorkMult = kernelWorkMultBuffer.getLong(0);
        }

        sharesSpace =_sharesSpace;
    }

    public cl_kernel getOCLKernel()
    {
        return oclKernel;
    }

    @Deprecated
    public String getKernelPTXFileName()
    {
        return kernelPTXFileName;
    }

    public String getKernelName()
    {
        return kernelName;
    }

    public long getKernelMaxWorkGroupSize()
    {
        return kernelMaxWorkGroupSize;
    }
    public long getKernelWorkMult(){return kernelWorkMult;}

    public int getSharesSpace()
    {
        return sharesSpace;
    }
}
