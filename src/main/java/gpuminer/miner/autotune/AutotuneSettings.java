package gpuminer.miner.autotune;

import gpuminer.GPULogging;
import gpuminer.miner.kernel.KernelType;
import logging.IAmpexLogger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class AutotuneSettings
{
    public static final String AUTOTUNE_VERSION = "timed autotune 4.0";

    public AutotuneSettings(String _version, String _deviceName, int _threadFactor, KernelType _kernelType)
    {
        version = _version;
        threadFactor = _threadFactor;
        kernelType = _kernelType;
        deviceName = _deviceName;
    }

    //TODO: this doesn't follow dependency injection, we should probably change these to private and put getters
    public int threadFactor;
    public KernelType kernelType;
    public String deviceName;
    public String version;

    public static AutotuneSettings fromJSONString(String JSON)
    {
        JSONObject jo = null;
        try
        {
            jo = (JSONObject) new JSONParser().parse(JSON);
        } catch (Exception e)
        {
            IAmpexLogger logger = GPULogging.getLogger();
            if (logger != null)
            {
                logger.error("", e);
            }
        }
        if (jo == null)
        {
            return null;
        }

        String version = (String) jo.get("version");
        String device = (String) jo.get("device");
        String threadFactorValue = (String) jo.get("threadFactor");
        String kernelName = (String) jo.get("kernel");

        int a = 0;

        if (version != null && version.equals(AUTOTUNE_VERSION) && device != null && threadFactorValue != null && kernelName != null)
        {
            try
            {
                int threadFactor = Integer.parseInt(threadFactorValue);
                KernelType kernel = KernelType.valueOf(kernelName);

                return new AutotuneSettings(version, device, threadFactor, kernel);
            }catch (Exception e)
            {
                return null;
            }
        }
        return null;
    }

    public String toJSONString()
    {
        JSONObject jo = new JSONObject();
        jo.put("version", version);
        jo.put("device", deviceName);
        if(kernelType != null)
        {
            jo.put("kernel", kernelType.name());
        }
        jo.put("threadFactor", "" + threadFactor);
        return jo.toJSONString();
    }
}
