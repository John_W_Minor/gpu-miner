package gpuminer.miner.autotune;

import gpuminer.GPULogging;
import gpuminer.JOCL.ops.JOCLAutotuneOp;
import gpuminer.data.JSONManager;
import gpuminer.data.StringFileHandler;
import gpuminer.miner.SHA3.SHA3Miner;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.ops.IAutotuneOp;
import gpuminer.miner.synchron.Synchron;
import logging.IAmpexLogger;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Queue on 2/12/2018.
 */
public class TimedAutotune {
    private static final byte[] DIFF = new byte[64];

    private static volatile Map<String, AutotuneSettings> autotuneSettingsMap = new HashMap<>();

    public synchronized static void setup(ArrayList<DeviceContext> _contexts, boolean _forceAutotune) {
        setup(_contexts, _forceAutotune, true);
    }

    public synchronized static void setup(ArrayList<DeviceContext> _contexts, boolean _forceAutotune, boolean _greedy) {
        IAmpexLogger logger = GPULogging.getLogger();
        if (logger != null) {
            System.out.println();
            logger.info("Reading autotune.settings file.");
        }

        StringFileHandler sfh = new StringFileHandler("autotune.settings");
        if (sfh.getLine(0) != null) {
            if (logger != null) {
                logger.info("Deserializing string map from autotune.settings file.");
            }

            Map<String, String> stringMap = JSONManager.parseJSONtoMap(sfh.getLine(0));
            if (stringMap != null) {
                for (String key : stringMap.keySet()) {
                    AutotuneSettings setting = AutotuneSettings.fromJSONString(stringMap.get(key));
                    if (setting != null) {
                        if (logger != null) {
                            logger.info("Setting deserialized.");
                        }
                        autotuneSettingsMap.put(setting.deviceName, setting);
                    }
                }
            }
        }


        for (DeviceContext context : _contexts) {
            if (!autotuneSettingsMap.containsKey(context.getDInfo().getDeviceName()) || _forceAutotune) {
                if (logger != null) {
                    System.out.println();
                    if (_greedy) {
                        logger.info("Beginning greedy autotune for " + context.getDInfo().getDeviceName() + ".");
                    } else {
                        logger.info("Beginning normal autotune for " + context.getDInfo().getDeviceName() + ".");
                    }
                }
                AutotuneSettings settings = new TimedAutotune().autotuneDevice(context, _greedy);
                autotuneSettingsMap.put(context.getDInfo().getDeviceName(), settings);
            }
        }

        if (logger != null) {
            logger.info("Serializing settings to autotune.settings file.");
        }
        Map<String, String> serializationMap = new HashMap<>();
        for (AutotuneSettings autotuneSettings : autotuneSettingsMap.values()) {
            if (logger != null) {
                logger.info("Setting serialized.");
            }
            serializationMap.put(autotuneSettings.deviceName, autotuneSettings.toJSONString());
        }

        if (logger != null) {
            logger.info("Saving autotune.settings file.");
        }
        sfh.replaceLine(0, JSONManager.parseMapToJSON(serializationMap).toJSONString());
        sfh.save();
    }

    public synchronized static Map<String, AutotuneSettings> getAutotuneSettingsMap() {
        return autotuneSettingsMap;
    }

    private synchronized AutotuneSettings autotuneDevice(DeviceContext _context, boolean _greedy) {
        long startTime = System.currentTimeMillis();

        IAmpexLogger logger = GPULogging.getLogger();
        if (logger != null) {
            System.out.println();
        }
        int maxThreadFactor = 1;
        try {
            int oclMaxTF = findMaxThreadFactor(.003, KernelType.OCL_MAIN, _context, _greedy);
//            int cudaMaxTF = findMaxThreadFactor(.01, KernelType.CUDA_MAIN, _context, _greedy);

            maxThreadFactor = oclMaxTF;//Math.max(oclMaxTF, cudaMaxTF);
        } catch (Exception e) {
            if (logger != null) {
                logger.error("", e);
            }

        }

        if (logger != null) {
            System.out.println();
            logger.info("Max thread factor for " + _context.getDInfo().getDeviceName() + " is " + maxThreadFactor + ".");
        }

        KernelType kernel = null;

//        if (_context.isKernelCompatible(KernelType.CUDA_MAIN))
//        {
//            kernel = KernelType.CUDA_MAIN;
//        } else
        if (_context.isKernelCompatible(KernelType.OCL_MAIN)) {
            kernel = KernelType.OCL_MAIN;
        }

        int optimalThreadFactor = 1;

        if (kernel != null) {
            optimalThreadFactor = findOptimalThreadFactor(maxThreadFactor, 125, kernel, _context, _greedy);
        }

        kernel = findOptimalKernel(optimalThreadFactor, _context, _greedy);

        if (kernel != null) {
            if (logger != null) {
                System.out.println();
                logger.info(kernel.name() + " is the fastest kernel on " + _context.getDInfo().getDeviceName() + ".");
            }
        }

        long endTime = System.currentTimeMillis() - startTime;

        if (logger != null) {
            System.out.println();
            logger.info("Autotuning " + _context.getDInfo().getDeviceName() + " took " + endTime + " milliseconds.");
        }

        return new AutotuneSettings(AutotuneSettings.AUTOTUNE_VERSION, _context.getDInfo().getDeviceName(), optimalThreadFactor, kernel);
    }

    private synchronized int findMaxThreadFactor(double _targetTimePerHash, KernelType _kernel, DeviceContext _context, boolean _greedy) throws Exception {
        int polls = 1000;

        IAutotuneOp autoTuneOp = null;

        if (_context.isKernelCompatible(_kernel)) {
//            if (_kernel.getType() == Constants.OCL_KERNEL)
//            {
            autoTuneOp = new JOCLAutotuneOp(_context.getOCLContext(), _greedy);
//            } else
//            {
//                autoTuneOp = new JCUDAAutotuneOp(_context.getCUDAContext(), _greedy);
//            }
        } else {
            return 1;
        }

        double time = 0;

        for (int i = 0; i < polls; i++) {
            time += autoTuneOp.time();
        }

        autoTuneOp.shutdown();

        double averageTime = time / polls;

        IAmpexLogger logger = GPULogging.getLogger();
        if (logger != null) {
            logger.info("Average launch time for " + _kernel.name() + " kernel on " + _context.getDInfo().getDeviceName() + " is " + averageTime + " nanoseconds.");
        }

        double threads = averageTime / _targetTimePerHash;

        double threadFactor = threads / Constants.WAVEFRONT;

        return (int) threadFactor;
    }

    private int lastThreadFactor = -1;
    private final Object optimalLock = new Object();
    private long tempKernelTime = 0;
    private long currentKernelTime = 0;
    private long startTime = 0;
    private long cumulativeKernelTime = 0;
    private long polls = 0;
    private int testThreadFactor = 0;

    private double intensity = .001;

    /**
     * redesigned to make use of reactive design shit BUT this requires very precise timing, so it still blocks until shits done
     *
     * @param _maxThreadfactor maximum thread factor for the device
     * @param _maxTime         maximum time to test (ms) per round
     * @param _kernel          kernel to test on
     * @param _context         device context to use for the miner
     * @param _greedy          whether to tune with greedy kernels/thread settings
     * @return optimal thread factor for GPU
     */
    private int findOptimalThreadFactor(int _maxThreadfactor, long _maxTime, KernelType _kernel, DeviceContext _context, boolean _greedy) {

        if (!_context.isKernelCompatible(_kernel)) {
            return 1;
        }

        IAmpexLogger logger = GPULogging.getLogger();

        SHA3Miner testMiner = new SHA3Miner(_context, DIFF, null, _maxThreadfactor, _kernel, _greedy);

//        for (double intensity = .001; intensity < 1.2; intensity *= 1.4) {
        //System.out.println("\nIntensity is " + intensity + ".");

        testMiner.getKernelTime().registerWatcher((w, v) -> {
            tempKernelTime = v;
//                System.out.println("temp kernel time: " + tempKernelTime + " current kernel time: " + currentKernelTime);
            if (tempKernelTime != currentKernelTime && tempKernelTime != -1) {
                currentKernelTime = tempKernelTime;
                cumulativeKernelTime += currentKernelTime;
                polls++;
            }

            long currentTime = System.currentTimeMillis() - startTime;

            if (currentTime > 500 && polls > 1) {
//                System.out.println("pausing mining");
                testMiner.pauseMining();
            }
        }, false);

        testMiner.isMining().registerWatcher((w, v) -> {
            if (!v) {
                long averageKernelTime = -1;

                if (polls != 0) {
                    averageKernelTime = cumulativeKernelTime / polls;
                }

                if (logger != null) {
                    System.out.println();
                    logger.info("Average kernel time for " + _kernel.name() + " with a test thread factor of " + testThreadFactor + " is " + averageKernelTime + " milliseconds.");
                }

                if (averageKernelTime > _maxTime) {

                    if (logger != null) {
                        System.out.println();
                        logger.info("Optimal thread factor for " + _context.getDInfo().getDeviceName() + " is " + lastThreadFactor + ".");
                    }
//                    testMiner.shutdown();
                    synchronized (optimalLock) {
                        optimalLock.notifyAll();
                    }
                    return;

                }

                lastThreadFactor = testThreadFactor;
                intensity *= 1.4;

//                System.out.println("new intensity: " + intensity);
                if (intensity >= 1.2) {
//                    System.out.println("final intensity: " + intensity);
//                    testMiner.shutdown();
                    synchronized (optimalLock) {
                        optimalLock.notifyAll();
                    }
                    return;
                }
                startMinerThread(testMiner, _maxThreadfactor);

            }
        }, false);

        startMinerThread(testMiner, _maxThreadfactor);
//        }

        try {
            synchronized (optimalLock) {
                optimalLock.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            return 0;
        } finally {
            testMiner.getKernelTime().clearWatchers();
            testMiner.isMining().clearWatchers();
            testMiner.shutdown();
            timingThread.interrupt();

        }

        if (logger != null) {
            System.out.println();
            logger.info("Final optimal thread factor for " + _context.getDInfo().getDeviceName() + " is " + lastThreadFactor + ".");
        }
        return lastThreadFactor;
    }
    private Thread timingThread;
    private void startMinerThread(SHA3Miner testMiner, long _maxThreadfactor) {
        timingThread = new Thread(() -> {
            testMiner.setIntensity(intensity, true);
            double inten = intensity;
            if (inten > 1) {
                inten = 1;
            }
            testThreadFactor = (int) (_maxThreadfactor * inten);

            if (testThreadFactor < 1) {
                testThreadFactor = 1;
            }
            startTime = System.currentTimeMillis();
            testMiner.resumeMining("Autotune!".getBytes(Constants.UTF8));
        });
        timingThread.start();
    }

    private long currentHPS = 0;

    private long cumulativeHPS = 0;
    private long hashratePolls = 0;

    private long bestHPS = 0;

    private KernelType fastestKernel = null;
    private final Object kernelLock = new Object();

    private int kernelIndex = 0;
    private KernelType kernel;

    private Thread miningThread;
    private synchronized KernelType findOptimalKernel(final int _maxThreadFactor, DeviceContext _context, boolean _greedy) {
        IAmpexLogger logger = GPULogging.getLogger();

        final int testThreadFactor = _maxThreadFactor;

        if (testThreadFactor < 1) {
            throw new RuntimeException("Test thread factor set to less than one for autotune");
        }

        kernel = KernelType.values()[0];
        SHA3Miner testMiner = new SHA3Miner(_context, DIFF, null, testThreadFactor, kernel, _greedy);

//                long tempHPS;
//                long currentHPS = 0;
//
//                long cumulativeHPS = 0;
//                long polls = 0;

        testMiner.isMining().registerWatcher((w, v) -> {
            if (!v) {
                long averageHPS = -1;

                if (hashratePolls != 0) {
                    averageHPS = cumulativeHPS / hashratePolls;
                }

                if (logger != null) {
                    logger.info("Average hashrate is " + NumberFormat.getNumberInstance().format(averageHPS) + " hashes per second.");
                }

                if (averageHPS > bestHPS) {
                    fastestKernel = kernel;
                    bestHPS = averageHPS;
                }
                kernelIndex++;
                if (kernelIndex >= KernelType.values().length) {
                    synchronized (kernelLock) {
                        kernelLock.notifyAll();
                    }
                    return;
                }
                kernel = KernelType.values()[kernelIndex];
                if (_context.isKernelCompatible(kernel)) {
                    if (logger != null) {
                        System.out.println();
                        logger.info("Testing " + kernel.name() + " kernel on " + _context.getDInfo().getDeviceName() + " with a test thread factor of " + testThreadFactor + ".");
                    }
                }
                miningThread = new Thread(() -> {
                    testMiner.compileNewKernel(kernel);
                    startTime = System.currentTimeMillis();
                    testMiner.resumeMining("Autotune!".getBytes(Constants.UTF8));
                });
                miningThread.start();
            }
        }, false);
        testMiner.getHashesPerSecond().registerWatcher((w, v) -> {

            if (v != currentHPS && v != -1) {
                currentHPS = v;
                cumulativeHPS += currentHPS;
                hashratePolls++;
            }

            long currentTime = System.currentTimeMillis() - startTime;

            if (currentTime > 1000 && hashratePolls > 1) {
                testMiner.pauseMining();
            }
        }, false);

        miningThread = new Thread(() -> {
            startTime = System.currentTimeMillis();
            testMiner.resumeMining("Autotune!".getBytes(Constants.UTF8));
        });
        miningThread.start();


        try {
            synchronized (kernelLock) {
                kernelLock.wait();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            testMiner.isMining().clearWatchers();
            testMiner.getHashesPerSecond().clearWatchers();
            testMiner.shutdown();
            miningThread.interrupt();
        }

        return fastestKernel;
    }
}
