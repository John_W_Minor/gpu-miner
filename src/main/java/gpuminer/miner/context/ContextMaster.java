package gpuminer.miner.context;

import gpuminer.GPULogging;
import gpuminer.JOCL.context.JOCLContextAndCommandQueue;
import gpuminer.JOCL.context.JOCLContextMaster;
import gpuminer.JOCL.ops.JOCLAutotuneOp;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.ops.IAutotuneOp;
import logging.IAmpexLogger;

import java.util.ArrayList;

/**
 * Created by Queue on 11/17/2017.
 */
public class ContextMaster {
    private static volatile boolean enableOCL = true;
//    private static volatile boolean enableCUDA = false;

    private volatile ArrayList<DeviceContext> contexts = new ArrayList<>();
    private volatile ArrayList<JOCLContextAndCommandQueue> oclContexts = null;
//    private volatile ArrayList<JCUDAContext> cudaContexts = null;

    private volatile JOCLContextMaster joclContextMaster = null;

//    private volatile JCUDAContextMaster jcudaContextMaster = null;

    private volatile byte deviceContextCount = 48;

    public ContextMaster() {
        IAmpexLogger logger = GPULogging.getLogger();

        if (enableOCL) {
            try {
                if (logger != null) {
                    System.out.println();
                    logger.info("Searching for compatible OCL-enabled devices...");
                }

                joclContextMaster = new JOCLContextMaster();

                oclContexts = joclContextMaster.getOCLContexts();

                if (logger != null) {
                    logger.info("Testing OCL Stability.");
                }

                for (JOCLContextAndCommandQueue oclctx : oclContexts) {
                    IAutotuneOp testOp = new JOCLAutotuneOp(oclctx, false);
                    testOp.time();
                    testOp.shutdown();
                }

                if (logger != null) {
                    logger.info("OCL Stability check finished.");
                }

                if (logger != null) {
                    logger.info("Found " + oclContexts.size() + " OCL-enabled device(s).");
                }

            } catch (Exception | LinkageError e) {
                if (logger != null) {
                    logger.warn("There is an issue with your OpenCL drivers, or your devices, or you do not have any compatible OpenCL-enabled devices.");
                    logger.error("", e);
                }
                joclContextMaster = null;
                oclContexts = null;
                disableOCL();
                //enableOCL = false;
            }
        }

//        if (false) {
//            try {
//                if (logger != null) {
//                    System.out.println();
//                    logger.info("Searching for compatible CUDA-enabled devices...");
//                }
//
//                jcudaContextMaster = new JCUDAContextMaster();
//
//                cudaContexts = jcudaContextMaster.getCUDAContexts();
//
//                if (logger != null) {
//                    logger.info("Testing CUDA Stability.");
//                }
//
////                for (JCUDAContext cudactx : cudaContexts)
////                {
////                    IAutotuneOp testOp = new JCUDAAutotuneOp(cudactx, false);
////                    testOp.time();
////                    testOp.shutdown();
////                }
//
////                if(logger != null)
////                {
////                    logger.info("CUDA Stability check finished.");
////                }
//
////                if(logger != null)
////                {
////                    logger.info("Found " + cudaContexts.size() + " CUDA-enabled device(s).");
////                }
//
//            } catch (Exception | LinkageError e) {
//                if (logger != null) {
//                    logger.warn("There is an issue with your CUDA drivers, or your devices, or you do not have any compatible CUDA-enabled devices.");
//                    logger.error("", e);
//                }
//                jcudaContextMaster = null;
//                cudaContexts = null;
//                //enableCUDA = false;
//            }


            grabOCLContexts();


        if (logger != null) {
            System.out.println();
            logger.info("Found " + contexts.size() + " total compatible device(s).");
        }
    }

    private void grabOCLContexts() {
        for (JOCLContextAndCommandQueue oclContext : oclContexts) {
            contexts.add(new DeviceContext(oclContext, deviceContextCount));

            deviceContextCount++;

            if (deviceContextCount > 90) {
                deviceContextCount = 48;
            }
        }
    }

    public synchronized ArrayList<DeviceContext> getContexts() {
        if (contexts != null) {
            return (ArrayList<DeviceContext>) contexts.clone();
        }
        return null;
    }

    public synchronized void shutdown() {
        //CUDA uses contexts differently, so only OCL needs its contexts shutdown.
        if (joclContextMaster != null && enableOCL) {
            try {
                joclContextMaster.shutdown();
            } catch (Exception e) {
                disableOCL();

                IAmpexLogger logger = GPULogging.getLogger();
                if (logger != null) {
                    logger.error("", e);
                }
            }
        }

        joclContextMaster = null;

        oclContexts = null;
        contexts = null;
    }

    public static synchronized void disableMining() {
        enableOCL = false;
    }

    public static synchronized void disableOCL() {
        enableOCL = false;
    }


    public static synchronized boolean canMine() {
        return enableOCL;

    }
}
