package gpuminer.miner.context;

import gpuminer.JOCL.context.JOCLContextAndCommandQueue;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.infocontainers.DeviceInfo;
import gpuminer.miner.kernel.KernelType;

/**
 * Created by Queue on 11/16/2017.
 */
public class DeviceContext {
    private JOCLContextAndCommandQueue oclContext = null;

    private DeviceInfo oclDInfo = null;

    private byte uniqueID = 0;

    public DeviceContext(JOCLContextAndCommandQueue _oclContext, byte _uniqueID) {
        oclContext = _oclContext;
        oclDInfo = oclContext.getDInfo();

        uniqueID = _uniqueID;
    }

    public JOCLContextAndCommandQueue getOCLContext() {
        return oclContext;
    }

    public boolean isKernelCompatible(KernelType _kernelType) {
        boolean isOCLKernel = _kernelType.getType() == Constants.OCL_KERNEL;

        boolean doesOCLCTXExist = oclContext != null;

        return (doesOCLCTXExist && isOCLKernel);
    }

    public DeviceInfo getDInfo() {
        return oclDInfo;

    }

    public DeviceInfo getOclDInfo() {
        return oclDInfo;
    }

    public byte getUniqueID() {
        return uniqueID;
    }
}
