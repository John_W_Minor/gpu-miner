package gpuminer.miner.databuckets;

/**
 * Created by Queue on 11/30/2017.
 */
public class GPUOutput
{
    private int block = 0;
    private int[] shares = null;

    public GPUOutput(int _block, int[] _shares)
    {
        block=_block;
        if(_shares!=null)
        {
            shares = _shares.clone();
        }
    }

    public int getBlock()
    {
        return block;
    }

    public int[] getShares()
    {
        if(shares!=null)
        {
            return shares.clone();
        }
        return null;
    }
}
