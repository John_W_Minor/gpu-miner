package gpuminer.miner.databuckets;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

/**
 * Created by Queue on 11/30/2017.
 */
public class BlockAndSharePayloads
{
    private Payload blockPayload = null;
    private Payload[] sharePayloads = null;

    public BlockAndSharePayloads(boolean _isLittleEndian, byte[] _speedPadding, GPUOutput _data)
    {
        int blockInt = _data.getBlock();

        if (blockInt != 0)
        {
            blockPayload = buildPayload(_isLittleEndian, _speedPadding, blockInt);
        } else
        {
            blockPayload = null;
        }

        int[] shareInts = _data.getShares();

        ArrayList<Payload> sharesList = new ArrayList<>();

        for (int i = 0; i < shareInts.length; i++)
        {
            if (shareInts[i] != 0)
            {
                sharesList.add(buildPayload(_isLittleEndian, _speedPadding, shareInts[i]));
            }
        }

        if (!sharesList.isEmpty())
        {
            int count = sharesList.size();
            sharePayloads = new Payload[count];
            for (int i = 0; i < count; i++)
            {
                sharePayloads[i] = sharesList.get(i);
            }
        } else
        {
            sharePayloads = null;
        }
    }

    //payload building methods
    private Payload buildPayload(boolean _isLittleEndian, byte[] _speedPadding, int _data)
    {
        ByteBuffer partialPayloadBuffer = ByteBuffer.allocate(4);

        if (_isLittleEndian)
        {
            partialPayloadBuffer.order(ByteOrder.LITTLE_ENDIAN);
        }

        //This makes the partial payload JSON safe.
        //This same conversion is done in the GPGPU kernel, so it must be done here too.
        //byte[] partialPayloadArray = partialPayloadBuffer.putInt(_data).array();
        byte[] partialPayloadArray = makeBytesJSONSafe(partialPayloadBuffer.putInt(_data).array());

        byte[] fullPayloadArray = ByteBuffer.allocate(_speedPadding.length + partialPayloadArray.length).put(_speedPadding).put(partialPayloadArray).array();

        return new Payload(fullPayloadArray);
    }

    ///*
    private byte[] makeBytesJSONSafe(byte[] _bytes)
    {
        byte[] split = splitBytes(_bytes);

        for (int i = 0; i < split.length; i++)
        {
            split[i] += 0x30;
        }

        return split;
    }

    private byte[] splitBytes(byte[] _bytes)
    {
        int size = _bytes.length;
        byte[] split = new byte[2 * size];

        for (int i = 0; i < size; i++)
        {
            split[i * 2] = (byte) ((_bytes[i] & 0x00ff) >> 4);
            split[(i * 2) + 1] = (byte) (_bytes[i] & 0x000f);
        }

        return split;
    }
    //*/

    //getters
    public Payload getBlockPayload()
    {
        return blockPayload;
    }

    public Payload[] getSharePayloads()
    {
        if (sharePayloads != null)
        {
            return sharePayloads.clone();
        }
        return null;
    }

    public boolean wasBlockFound()
    {
        return blockPayload != null;
    }

    public boolean wasShareFound()
    {
        return sharePayloads != null;
    }
}
