package gpuminer.miner.databuckets;

/**
 * Created by Queue on 11/30/2017.
 */
public class Payload
{
    private byte[] bytes = null;

    public Payload(byte[] _bytes)
    {
        bytes=_bytes.clone();
    }

    public byte[] getBytes()
    {
        return bytes.clone();
    }
}
