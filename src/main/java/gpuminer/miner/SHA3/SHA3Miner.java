package gpuminer.miner.SHA3;

import com.ampex.amperabase.Utils;
import com.ampex.amperabase.data.BaseValueWatchable;
import com.ampex.amperabase.data.WBoolean;
import com.ampex.amperabase.data.WList;
import com.ampex.amperabase.data.WLong;
import gpuminer.GPULogging;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.context.ContextMaster;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.databuckets.BlockAndSharePayloads;
import gpuminer.miner.infocontainers.KernelInfo;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.ops.MiningOp;
import logging.IAmpexLogger;

/**
 * Created by Queue on 10/5/2017.
 */
public class SHA3Miner {
    private volatile SHA3MinerThread miningThread = null;

    private DeviceContext context = null;

    private volatile MiningOp miningOp = null;

    private String deviceName = null;
    private String vendor = null;
    private byte[] blockDifficulty;
    private byte[] shareDifficulty;
    private KernelType kernelType = null;
    private boolean greedy;

    private int threadFactor = 0;

    private int maxThreads = 1;
    private volatile double intensity = 1;
    private WBoolean isMining = new WBoolean();

    public SHA3Miner(DeviceContext _context, byte[] _blockDifficulty, byte[] _shareDifficulty, int _threadFactor, KernelType _kernelType) {
        this(_context, _blockDifficulty, _shareDifficulty, _threadFactor, _kernelType, false);
    }

    public SHA3Miner(DeviceContext _context, byte[] _blockDifficulty, byte[] _shareDifficulty, int _threadFactor, KernelType _kernelType, boolean _greedy) {
        blockDifficulty = _blockDifficulty;
        shareDifficulty = _shareDifficulty;
        kernelType = _kernelType;
        greedy = _greedy;

        if (ContextMaster.canMine()) {
            context = _context;

            deviceName = context.getDInfo().getDeviceName();
            vendor = context.getDInfo().getVendor();

            threadFactor = _threadFactor;
            maxThreads = calculateThreads(threadFactor);

            if (ContextMaster.canMine()) {
                try {
                    miningOp = new MiningOp(context, _blockDifficulty, _shareDifficulty, maxThreads, kernelType, _greedy);

                    miningThread = new SHA3MinerThread(context, miningOp, maxThreads, kernelType);
                    miningThread.isGPURunning().registerWatcher((w, v) -> isMining.set(v && !miningThread.isMiningPaused().get()), true);
                    miningThread.isMiningPaused().registerWatcher((w, v) -> isMining.set(!v && miningThread.isGPURunning().get()), true);
                } catch (Exception e) {
                    ContextMaster.disableMining();

                    IAmpexLogger logger = GPULogging.getLogger();
                    if (logger != null) {
                        logger.error("", e);
                    }
                }
            }
        }
    }

    public synchronized void updateShareDiff(byte[] shareDifficulty)
    {
//        System.out.println("New share diff: " + Utils.toHexArray(shareDifficulty));
//        System.out.println("Size of array: " + shareDifficulty.length);
        if(shareDifficulty == null)
        {
            this.shareDifficulty = null;
            recompileKernel(blockDifficulty,null);
            return;
        }
        byte[] sd = new byte[64];
        int p = 63;
        for(int i = shareDifficulty.length-1; i >= 0; i--)
        {
            sd[p] = shareDifficulty[i];
            p--;
        }
        this.shareDifficulty = sd;
        System.out.println("New share diff: " + Utils.toHexArray(sd));
        recompileKernel(blockDifficulty,this.shareDifficulty);
    }

    public synchronized void compileNewKernel(KernelType kernelType)
    {

        this.kernelType = kernelType;
        try {
            miningOp = new MiningOp(context, blockDifficulty, shareDifficulty, maxThreads, kernelType, greedy);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        miningThread = new SHA3MinerThread(context, miningOp, maxThreads, kernelType);
        miningThread.isGPURunning().registerWatcher((w, v) -> isMining.set(v && !miningThread.isMiningPaused().get()), true);
        miningThread.isMiningPaused().registerWatcher((w, v) -> isMining.set(!v && miningThread.isGPURunning().get()), true);

    }
    public synchronized void recompileKernel(byte[] _blockDifficulty, byte[] _shareDifficulty) {
        recompileKernel(_blockDifficulty, _shareDifficulty, false);
    }

    public synchronized void recompileKernel(byte[] _blockDifficulty, byte[] _shareDifficulty, boolean _greedy) {
        shutdown();

        if (ContextMaster.canMine()) {
            try {
                miningOp = new MiningOp(context, _blockDifficulty, _shareDifficulty, maxThreads, kernelType, _greedy);

                miningThread = new SHA3MinerThread(context, miningOp, maxThreads, kernelType);
            } catch (Exception e) {
                ContextMaster.disableMining();

                IAmpexLogger logger = GPULogging.getLogger();
                if (logger != null) {
                    logger.error("", e);
                }
            }
        }

        setIntensity(intensity);
    }

    private static int calculateThreads(int _threadFactor) {
        return _threadFactor * Constants.WAVEFRONT;
    }

    public void setIntensity(double _intensity, boolean _reset) {
        intensity = _intensity;

        if (intensity > 1) {
            intensity = 1;
        }

        int threads = (int) (calculateThreads(threadFactor) * intensity);

        if (threads < 1) {
            threads = 1;
        }

        miningThread.setThreads(threads, _reset);
    }

    public void setIntensity(double _intensity) {
        setIntensity(_intensity, false);
    }

    public synchronized DeviceContext getContext() {
        return context;
    }

    public synchronized KernelType getKernelType() {
        return kernelType;
    }

    public synchronized KernelInfo getKInfo() {

        return miningOp.getKInfo();

    }

    public synchronized void resumeMining(byte[] _message) {
        resumeMining(_message, null);
    }

    public synchronized void resumeMining(byte[] _message, byte[] _payloadEntropy) {

        miningThread.resume(_message, _payloadEntropy);

    }

    public void pauseMining() {

        miningThread.pause();

    }

    public BlockAndSharePayloads[] getPayloads() {

        return miningThread.getPayloads();

    }

    public int getThreads() {

        return miningThread.getThreads();

    }

    public BaseValueWatchable<Long> getKernelTime() {

        return miningThread.getKernelTime();

    }

    public BaseValueWatchable<Long> getHashesPerSecond() {

        return miningThread.getHashesPerSecond();

    }


    public WBoolean isMining() {
        return isMining;
    }

    //TODO stop null checking and pass this in, this is not a good way to handle this
    private final WBoolean fal = new WBoolean();

    public BaseValueWatchable<Boolean> isGPUrunning() {
        if (miningThread != null) {
            return miningThread.isGPURunning();
        }
        return fal;
    }

    public void setGreedy(boolean val) {
        miningOp.setGreedy(val);
    }

    public void shutdown() {
        if (miningThread != null) {
            miningThread.stop();
            //miningThread = null;
        }

        if (ContextMaster.canMine()) {
            try {
                miningOp.shutdown();
            } catch (Exception e) {
                ContextMaster.disableMining();

                IAmpexLogger logger = GPULogging.getLogger();
                if (logger != null) {
                    logger.error("", e);
                }
            }
        }
    }
}
