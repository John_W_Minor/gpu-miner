package gpuminer.miner.SHA3;

import com.ampex.amperabase.data.BaseValueWatchable;
import com.ampex.amperabase.data.WBoolean;
import com.ampex.amperabase.data.WLong;
import gpuminer.GPULogging;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.context.ContextMaster;
import gpuminer.miner.context.DeviceContext;
import gpuminer.miner.databuckets.BlockAndSharePayloads;
import gpuminer.miner.databuckets.GPUOutput;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.ops.MiningOp;
import gpuminer.miner.synchron.Synchron;
import jcuda.driver.CUcontext;
import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import jcuda.runtime.JCuda;
import logging.IAmpexLogger;
import org.bouncycastle.crypto.digests.KeccakDigest;
import org.bouncycastle.jcajce.provider.digest.SHA3;

import java.lang.reflect.Field;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Random;

import static jcuda.driver.JCudaDriver.*;

/**
 * Created by Queue on 10/4/2017.
 */
public class SHA3MinerThread
{
    //private final Object NOTIFIER = new Object();

    private static final long INTENSITY_MAX_WAIT_MILLISECONDS = 40;

    private static final int MESSAGEBLOCKSIZE = 144;

    private static final long CYCLERANGE = (long) Math.pow(2, 32);


    private DeviceContext context = null;

    //private PrimingOp primerOp = null;
    private MiningOp minerOp = null;

    private volatile int threads = 0;

    private volatile int setThreads = 0;

    private int maxThreads = 0;

    private int computeUnits = 0;

    private KernelType kernelType = null;

    private volatile byte[] speedPadding = null;
    private volatile byte[] sponge = null;


//    private volatile Thread miningThread = null;


    //gpu outputs
    private volatile BaseValueWatchable<Long> hashesPerSecond = new BaseValueWatchable<>(-1L);

    private volatile ArrayList<BlockAndSharePayloads> payloadsList = new ArrayList<>();


    //flow control variables

    private volatile boolean persist = true;

    private volatile BaseValueWatchable<Boolean> miningPaused = new BaseValueWatchable<>(true);

    private volatile WBoolean GPURunning = new WBoolean();


    private volatile boolean threadSetupFinished = false;

    private volatile boolean threadShutdownFinished = false;


    //CUDA context stuff
//    private volatile CUcontext cudaMiningContext = null;
//
//    private volatile CUmodule cudaKernelModule = null;

//    private volatile CUfunction cudaKernelFunction = null;


    public SHA3MinerThread(DeviceContext _context, MiningOp _miningOp, int _threads, KernelType _kernelType)
    {
        //These are the services the miners needs to run OpenCL.
        context = _context;
        //primerOp = _primingOp;
        minerOp = _miningOp;

        //This tells the GPU how many hashes we want it to compute in a given batch.
        threads = _threads;
        setThreads = threads;
        maxThreads = threads;


        computeUnits = _context.getDInfo().getMaxComputeUnits();

        kernelType = _kernelType;

        //This is our thread.
//        miningThread = new Thread(this, _threadName);
//        miningThread.setDaemon(true);
//        miningThread.start();
        //We need to wait for the mining thread to finish setting up before we can let go of the thread that called start().
//        while (!threadSetupFinished)
//        {
//            Synchron.idle();
//        }
//        GPURunning.registerWatcher((w,v) -> System.out.println("GPURUNNING set to: " + v),false);
    }

    public void run()
    {
//        if (kernelType.getType() == Constants.CUDA_KERNEL && ContextMaster.canMine(kernelType))
//        {
//            try
//            {
//                cudaMiningContext = new CUcontext();
//                cuCtxCreate(cudaMiningContext, JCuda.cudaDeviceScheduleYield, context.getCUDAContext().getDevice());
//
//                cudaKernelModule = new CUmodule();
//                cuModuleLoad(cudaKernelModule, minerOp.getKInfo().getKernelPTXFileName());
//
//                cudaKernelFunction = new CUfunction();
//                cuModuleGetFunction(cudaKernelFunction, cudaKernelModule, minerOp.getKInfo().getKernelName());
//            } catch (Exception e)
//            {
//                cudaMiningContext = null;
//                cudaKernelModule = null;
//                cudaKernelFunction = null;
//
//                persist = false;
//
//                ContextMaster.disableMining(kernelType);
//
//                IAmpexLogger logger = GPULogging.getLogger();
//                if (logger != null)
//                {
//                    logger.error("", e);
//                }
//            }
//        }
//
//        threadSetupFinished = true;
//
//        while (persist && ContextMaster.canMine(kernelType))
//        {
//            try
//            {
//                mine();
//            } catch (Exception e)
//            {
//                persist = false;
//
//                ContextMaster.disableMining(kernelType);
//
//                IAmpexLogger logger = GPULogging.getLogger();
//                if (logger != null)
//                {
//                    logger.error("", e);
//                }
//            }
//
//            Synchron.idle();
//            /*
//            if (!shouldMine())
//            {
//                try
//                {
//                    synchronized (NOTIFIER)
//                    {
//                        //long startTime = System.nanoTime();
//                        NOTIFIER.wait();
//                        //long endTime = System.nanoTime() - startTime;
//                        //System.out.println(endTime);
//                    }
//                } catch (Exception e)
//                {
//                    Logger logger = GPULogging.getLogger();
//                    if(logger != null)
//                    {
//                        logger.error("",e);
//                    }
//                }
//            }
//            //*/
//        }
//
//        //If the miner is using a CUDA kernel we need to dispose the CUDA context and module.
//        if (kernelType.getType() == Constants.CUDA_KERNEL && ContextMaster.canMine(kernelType))
//        {
//            if (cudaKernelModule != null)
//            {
//                try
//                {
//                    cuModuleUnload(cudaKernelModule);
//                    cuCtxDestroy(cudaMiningContext);
//                } catch (Exception e)
//                {
//                    ContextMaster.disableMining(kernelType);
//
//                    IAmpexLogger logger = GPULogging.getLogger();
//                    if (logger != null)
//                    {
//                        logger.error("", e);
//                    }
//                }
//            }
//        }
//
//        threadShutdownFinished = true;
    }

    public void setThreads(int _threads, boolean _reset)
    {
        if (_threads < 1)
        {
            _threads = 1;
        }

        if(_reset)
        {
            maxThreads = _threads;
        }else if (_threads > maxThreads)
        {
            _threads = maxThreads;
        }

        setThreads = _threads;
    }

    private void mine() throws Exception
    {
        GPURunning.set(shouldMine());

        if (GPURunning.get() && persist && ContextMaster.canMine())
        {
            //int usedComputeUnits = computeUnits;//(int)(computeUnits * ((double)threads / maxThreads));
            //if(usedComputeUnits < 1)
            //{
                //usedComputeUnits = 1;
            //}

            GPUOutput data;
            BlockAndSharePayloads justMined;

            long currentPayload = 1;

            while ((currentPayload + threads) < CYCLERANGE && shouldMine())
            {
                long waitTime = INTENSITY_MAX_WAIT_MILLISECONDS - (INTENSITY_MAX_WAIT_MILLISECONDS * (threads / maxThreads));

                if (waitTime < 0)
                {
                    waitTime = 0;
                }

                Thread.sleep(waitTime);

                long startTime = System.nanoTime();

                //This grabs the output of the mining operation.
                data = minerOp.mine(threads, sponge, (int) currentPayload, computeUnits);

                long endTime = System.nanoTime() - startTime;

                justMined = new BlockAndSharePayloads(context.getDInfo().isLittleEndian(), speedPadding, data);

                if (justMined.wasBlockFound() || justMined.wasShareFound())
                {
                    payloadsList.add(justMined);
                }



                //For small sizes of threads sometimes this will be 0.
                if (endTime != 0)
                {
                    //System.out.println("kernel execution time: " + endTime/1000000);
                    endTime += (waitTime * 1_000_000);
                    hashesPerSecond.set((long) (threads) * 1_000_000_000 / endTime);
                }

                currentPayload += threads;

                if (justMined.wasBlockFound())
                {
                    miningPaused.set(true);
                }
            }

            //To make sure we fill out the entire cycle.
            //-1 because the hashes are zero indexed and we're looking for exactly how many are left in the cycle.
            if (currentPayload < (CYCLERANGE - 1) && shouldMine())
            {
                int tempThreads = (int) (CYCLERANGE - currentPayload - 1);

                data = minerOp.mine(tempThreads, sponge, (int) currentPayload, computeUnits);

                justMined = new BlockAndSharePayloads(context.getDInfo().isLittleEndian(), speedPadding, data);

                if (justMined.wasBlockFound() || justMined.wasShareFound())
                {
                    payloadsList.add(justMined);
                }
            }

            GPURunning.setNoProp(false);
            miningPaused.set(true);
        }
    }

    private boolean shouldMine()
    {
        return persist && !miningPaused.get() && ContextMaster.canMine();
    }

    public void resume(byte[] _message)
    {
        resume(_message, null);
    }

    public void resume(byte[] _message, byte[] _payloadEntropy)
    {
        pause();
//        System.out.println("Paused");
        payloadsList.clear();

        if (threads != setThreads)
        {
            minerOp.resetHeat();
            threads = setThreads;
        }
//        System.out.println("reset heat, whatever the fuck that means");
        if (_message != null && _message.length != 0 && ContextMaster.canMine())
        {
            byte uniqueID = context.getUniqueID();

            int payloadEntropySize = 0;
            if (_payloadEntropy != null)
            {
                payloadEntropySize = _payloadEntropy.length;

                byte[] payloadTemp = new byte[payloadEntropySize + 1];
                System.arraycopy(_payloadEntropy, 0, payloadTemp, 0, payloadEntropySize);

                payloadTemp[payloadEntropySize] = uniqueID;

                _payloadEntropy = payloadTemp;

                payloadEntropySize = _payloadEntropy.length;

                if (payloadEntropySize > MESSAGEBLOCKSIZE)
                {
                    _payloadEntropy = new byte[1];
                    _payloadEntropy[0] = uniqueID;
                    payloadEntropySize = 1;
                }
            } else
            {
                _payloadEntropy = new byte[1];
                _payloadEntropy[0] = uniqueID;
                payloadEntropySize = 1;
            }

            int speedPaddingSize = MESSAGEBLOCKSIZE - ((_message.length + payloadEntropySize) % MESSAGEBLOCKSIZE);

            if (speedPaddingSize != MESSAGEBLOCKSIZE)
            {
                speedPadding = new byte[speedPaddingSize + payloadEntropySize];
            } else
            {
                speedPadding = new byte[payloadEntropySize];
            }

            if (payloadEntropySize != 0)
            {
                System.arraycopy(_payloadEntropy, 0, speedPadding, 0, payloadEntropySize);
            }

            Random rand = new Random();

            for (int i = payloadEntropySize; i < speedPadding.length; i++)
            {
                speedPadding[i] = (byte) (0x47 + (rand.nextInt(20)));
            }

            byte[] messageWithSpeedPadding = ByteBuffer.allocate(_message.length + speedPadding.length).put(_message).put(speedPadding).array();

            try
            {

                //TODO fix this awful mess of reflection
                //long startTime = System.nanoTime();
                ///*
                SHA3.DigestSHA3 md = new SHA3.Digest512();
                md.update(messageWithSpeedPadding);

                Field field = md.getClass().getSuperclass().getSuperclass().getDeclaredField("digest");
                field.setAccessible(true);
                KeccakDigest keccakDigest = (KeccakDigest) field.get(md);
                field = keccakDigest.getClass().getSuperclass().getDeclaredField("state");
                field.setAccessible(true);

                ByteBuffer buffer = ByteBuffer.allocate(200);
                buffer.order(ByteOrder.LITTLE_ENDIAN);

                long[] keccakDigestLongArray = (long[]) field.get(keccakDigest);

                for (long lane : keccakDigestLongArray)
                {
                    buffer.putLong(lane);
                }

                sponge = buffer.array();
                //sponge = (byte[]) field.get(keccakDigest);
                //primerOp.prime(new byte[72 * 2]);
                //*/

                //sponge = primerOp.prime(messageWithSpeedPadding);
                //long endTime = System.nanoTime() - startTime;

                //System.out.println("Priming took " + endTime + " ns.\n");
            } catch (Exception e)
            {
                persist = false;

                ContextMaster.disableMining();

                IAmpexLogger logger = GPULogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
//            System.out.println("Did a bunch of shit");
            //This tells the mining thread to start working.
            miningPaused.set(false);

            if(shouldMine())
            try
            {
                mine();
            }catch(InterruptedException e1){
                miningPaused.setNoProp(true);
                return;
            } catch (Exception e)
            {
                persist = false;

                ContextMaster.disableMining();

                IAmpexLogger logger = GPULogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
            //synchronized (NOTIFIER)
            {
                //NOTIFIER.notify();
            }
//            System.out.println("other shit");
            //This waits for the mining thread to start working before releasing the lock on the SHA3Miner object that owns this object.
//            while (!GPURunning.get() && shouldMine())
//            {
//                Synchron.idle();
//            }
//            System.out.println("idling synchron, this shit's retarded");
        }
    }

    public void pause()
    {
        //This tells the miner thread to stop working.
        miningPaused.setNoProp(true);

        //This waits for the mining thread to stop working before releasing the lock on the SHA3Miner object that owns this object.
//        while (GPURunning.get())
//        {
//            Synchron.idle();
//        }
    }

    public void stop()
    {
        //This tells the miner to stop running.
        persist = false;
        miningPaused.set(true);
        //This waits for the mining thread to die before releasing the lock on the SHA3Miner object that owns this object.
//        while (!threadShutdownFinished)
//        {
//            Synchron.idle();
//        }

        //This throws away the thread so the GC can get it.
//        miningThread = null;
    }


    //getters
    public int getThreads()
    {
        return threads;
    }


    //gpu outputs
    public BaseValueWatchable<Long> getKernelTime()
    {
        return minerOp.getKernelTime();
    }

    public BaseValueWatchable<Long> getHashesPerSecond()
    {
        return hashesPerSecond;
    }

    public BlockAndSharePayloads[] getPayloads()
    {
        if (!payloadsList.isEmpty())
        {
            int count = payloadsList.size();
            BlockAndSharePayloads[] payloadsArray = new BlockAndSharePayloads[count];
            for (int i = 0; i < count; i++)
            {
                payloadsArray[i] = payloadsList.get(i);
            }
            return payloadsArray;
        }
        return null;
    }


    //flow control
    public BaseValueWatchable<Boolean> isMiningPaused()
    {
        return miningPaused;
    }

    public BaseValueWatchable<Boolean> isGPURunning()
    {
        return GPURunning;
    }
}