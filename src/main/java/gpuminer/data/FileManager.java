package gpuminer.data;

import gpuminer.GPULogging;
import logging.IAmpexLogger;

import java.io.File;
import java.io.IOException;

/**
 * Created by Bryan on 7/17/2017.
 */
public class FileManager implements IFileManager
{
    protected File file;

    public FileManager(String fileName)
    {
        file = new File(fileName);
        if (!file.exists()) try
        {
            if (file.getParentFile() != null)
            {
                if (!file.getParentFile().mkdirs())
                {

                    //return; TODO: investigate if we should return here or not
                }
            }
            if (!file.createNewFile())
            {
                return;
            }
        } catch (IOException e)
        {
            IAmpexLogger logger = GPULogging.getLogger();
            if(logger != null)
            {
                logger.error("", e);
            }
        }

    }

    @Override
    public boolean save()
    {
        return false;
    }

    @Override
    public boolean delete()
    {
        return file.delete();
    }

    public File getFile()
    {
        return file;
    }
}
