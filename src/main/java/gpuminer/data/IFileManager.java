package gpuminer.data;

/**
 * Created by Bryan on 7/17/2017.
 */
public interface IFileManager {

    boolean save();
    boolean delete();
}
