package gpuminer.JOCL.compiler;

import gpuminer.JOCL.context.JOCLContextAndCommandQueue;
import gpuminer.miner.infocontainers.KernelInfo;
import gpuminer.miner.kernel.KernelWriter;
import org.jocl.cl_kernel;
import org.jocl.cl_program;

import static org.jocl.CL.*;

/**
 * Created by Queue on 10/3/2017.
 */
public class JOCLKernelCompiler
{
    public static KernelInfo compileSourceToKernel(JOCLContextAndCommandQueue _oclContext, String _kernelName, String[] _filePaths, String _compileOptions) throws Exception
    {
        return compileSourceToKernel( _oclContext, _kernelName, _filePaths, _compileOptions, null, null, 0);
    }

    public static KernelInfo compileSourceToKernel(JOCLContextAndCommandQueue _oclContext, String _kernelName, String[] _filePaths, String _compileOptions, byte[] _blockDifficulty, byte[] _shareDifficulty, int _threads) throws Exception
    {
        cl_kernel kernel;

        StringBuilder source = new StringBuilder();

        KernelWriter.appendAMD_MEDIA_OPSIfApplicable(source, _oclContext.getDInfo());

        KernelWriter.appendBlockDifficulty(source, _blockDifficulty);

        KernelWriter.appendShareDifficulty(source, _shareDifficulty);

        int sharesSpace = KernelWriter.appendSharesSpace(source,_shareDifficulty,_threads);

        KernelWriter.appendSource(source, _filePaths);

        cl_program program = clCreateProgramWithSource(_oclContext.getContext(), 1, new String[]{source.toString()}, null, null);

        clBuildProgram(program, 0, null, _compileOptions, null, null);

        kernel = clCreateKernel(program, _kernelName, null);

        clReleaseProgram(program);

        return new KernelInfo(kernel, _oclContext.getDevice(), sharesSpace);
    }
}
