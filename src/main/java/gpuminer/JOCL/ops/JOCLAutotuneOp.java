package gpuminer.JOCL.ops;

import gpuminer.JOCL.compiler.JOCLKernelCompiler;
import gpuminer.JOCL.context.JOCLContextAndCommandQueue;
import gpuminer.miner.infocontainers.KernelInfo;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.ops.IAutotuneOp;
import org.jocl.*;

import static org.jocl.CL.*;

/**
 * Created by Queue on 1/14/2018.
 */
public class JOCLAutotuneOp implements IAutotuneOp
{
    //This is information the mining operation uses to grab its kernel.
    private static final String KERNEL_NAME = "sha3512miner";

    private static final String KERNEL_GREEDY_PATH = "kernels/GREEDY.minor";
    private static final String KERNEL_MAKE_AS_OCL_SOURCE_PATH = "kernels/MAKE AS OCL.minor";
    private static final String KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH = "kernels/KECCAK UNIVERSAL.minor";
    private static final String KERNEL_MINER_SOURCE_PATH = "kernels/SHA-3 512 MINER.minor";

    private JOCLContextAndCommandQueue oclContext = null;

    private KernelInfo kInfo = null;

    private int threads = 1;

    //sponge
    private static final int SPONGE_SIZE = 25 * Sizeof.cl_ulong; //Or 200*Sizeof.cl_uchar

    //work per work element
    private int[] workPerWorkElementArray = {1};

    //payload
    private int[] payloadArray = {0};

    //winning block
    private int[] winningBlockPayloadArray = {0};
    private int[] winningBlockPayloadOutput = new int[1];

    //shares
    private int sharesSpace = 0;
    private int[] sharesSpaceArray = null;
    private int sharesSpaceSize = 0;
    private int[] sharePayloadsOutput = null;

    private KernelType autotuneKernel = KernelType.OCL_MAIN;

    public JOCLAutotuneOp(JOCLContextAndCommandQueue _oclContext, boolean _greedy) throws Exception
    {
        oclContext = _oclContext;

        String kernelCompileOptions = (oclContext.getDInfo().isNvidia()) ? "-cl-nv-maxrregcount=256" : "";

        String[] kernelSourcePaths;

        if(_greedy)
        {
            kernelSourcePaths = new String[]{KERNEL_GREEDY_PATH, KERNEL_MAKE_AS_OCL_SOURCE_PATH, KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH, autotuneKernel.getSource(), KERNEL_MINER_SOURCE_PATH};
        } else
        {
            kernelSourcePaths = new String[]{KERNEL_MAKE_AS_OCL_SOURCE_PATH, KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH, autotuneKernel.getSource(), KERNEL_MINER_SOURCE_PATH};
        }

        byte[] blockDifficulty = new byte[64];
        blockDifficulty[63] = 0x01;

        kInfo = JOCLKernelCompiler.compileSourceToKernel(oclContext, KERNEL_NAME, kernelSourcePaths, kernelCompileOptions, blockDifficulty, null, threads);

        //shares
        sharesSpace = kInfo.getSharesSpace();
        sharesSpaceArray = new int[sharesSpace];
        sharesSpaceSize = sharesSpace * Sizeof.cl_uint;
        sharePayloadsOutput = new int[sharesSpace];
    }

    public long time() throws Exception
    {

        byte[] sponge = new byte[SPONGE_SIZE];
        //new Random().nextBytes(sponge);

        long startTime = System.nanoTime();

        long[] globalWork = {threads, 1, 1};

        long[] localWork = null;

        //Sponge allocations
        cl_mem spongeMemory = clCreateBuffer(oclContext.getContext(), CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, SPONGE_SIZE, Pointer.to(sponge), null);

        //Winner job allocation
        winningBlockPayloadArray[0] = 0;
        cl_mem winningBlockPayloadMemory = clCreateBuffer(oclContext.getContext(), CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, Sizeof.cl_int, Pointer.to(winningBlockPayloadArray), null);

        //Winning Share allocation


        cl_mem sharesSpaceMemory = clCreateBuffer(oclContext.getContext(), CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sharesSpaceSize, Pointer.to(sharesSpaceArray), null);

        //Sponge arguments
        clSetKernelArg(kInfo.getOCLKernel(), 0, Sizeof.cl_mem, Pointer.to(spongeMemory));

        //Work Per Work Element argument
        clSetKernelArg(kInfo.getOCLKernel(), 1, Sizeof.cl_int, Pointer.to(workPerWorkElementArray));

        //Payload argument
        clSetKernelArg(kInfo.getOCLKernel(), 2, Sizeof.cl_int, Pointer.to(payloadArray));

        //Winning block payload argument
        clSetKernelArg(kInfo.getOCLKernel(), 3, Sizeof.cl_mem, Pointer.to(winningBlockPayloadMemory));

        //sharesSpace argument
        clSetKernelArg(kInfo.getOCLKernel(), 4, Sizeof.cl_mem, Pointer.to(sharesSpaceMemory));

        cl_event hashingEvent = new cl_event();
        cl_event[] hashEventArray = {hashingEvent};

        clEnqueueNDRangeKernel(oclContext.getCommandQueue(), kInfo.getOCLKernel(), 1, null, globalWork, localWork, 0, null, hashingEvent);
        clFlush(oclContext.getCommandQueue());

        clWaitForEvents(1, hashEventArray);

        //Reads the winning job output and tells this thread to wait until it's done.
        cl_event readingEvent1 = new cl_event();

        clEnqueueReadBuffer(oclContext.getCommandQueue(), winningBlockPayloadMemory, CL_TRUE, 0, Sizeof.cl_uint, Pointer.to(winningBlockPayloadOutput), 0, null, readingEvent1);

        clWaitForEvents(1, new cl_event[]{readingEvent1});


        cl_event readingEvent2 = new cl_event();

        clEnqueueReadBuffer(oclContext.getCommandQueue(), sharesSpaceMemory, CL_TRUE, 0, sharesSpaceSize, Pointer.to(sharePayloadsOutput), 0, null, readingEvent2);

        clWaitForEvents(1, new cl_event[]{readingEvent2});

        //Release the cl_mem buffers.
        clReleaseMemObject(spongeMemory);
        clReleaseMemObject(winningBlockPayloadMemory);
        clReleaseMemObject(sharesSpaceMemory);

        //Releases the cl_event events.
        clReleaseEvent(hashingEvent);
        clReleaseEvent(readingEvent1);
        clReleaseEvent(readingEvent2);

        return System.nanoTime()-startTime;
    }

    public KernelInfo getKInfo()
    {
        return kInfo;
    }

    //Call this before sending the mining operation object to the GC.
    public void shutdown() throws Exception
    {
        cl_kernel kernel = kInfo.getOCLKernel();

        if (kernel != null)
        {
            clReleaseKernel(kernel);
        }
    }
}
