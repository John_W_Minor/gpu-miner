package gpuminer.JOCL.ops;

import com.ampex.amperabase.data.BaseValueWatchable;
import gpuminer.JOCL.compiler.JOCLKernelCompiler;
import gpuminer.JOCL.context.JOCLContextAndCommandQueue;
import gpuminer.miner.databuckets.GPUOutput;
import gpuminer.miner.infocontainers.DeviceInfo;
import gpuminer.miner.infocontainers.KernelInfo;
import gpuminer.miner.kernel.KernelType;
import gpuminer.miner.ops.IMiningOp;
import gpuminer.miner.synchron.Synchron;
import jcuda.driver.CUfunction;
import org.jocl.*;

import java.util.Random;

import static org.jocl.CL.*;

/**
 * Created by Queue on 10/3/2017.
 */
public class JOCLSHA3MiningOp implements IMiningOp
{
    //This is information the mining operation uses to grab its kernel.
    private static final String KERNEL_NAME = "sha3512miner";

    private static final String KERNEL_GREEDY_PATH = "kernels/GREEDY.minor";
    private static final String KERNEL_MAKE_AS_OCL_SOURCE_PATH = "kernels/MAKE AS OCL.minor";
    private static final String KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH = "kernels/KECCAK UNIVERSAL.minor";
    private static final String KERNEL_MINER_SOURCE_PATH = "kernels/SHA-3 512 MINER.minor";

    private JOCLContextAndCommandQueue oclContext = null;
    private byte[] blockDifficulty;
    private byte[] shareDifficulty;
    private int threads;

    private KernelInfo kInfo = null;

    private boolean greedy;

    private DeviceInfo dInfo = null;

    //sponge
    private static final int SPONGE_SIZE = 25 * Sizeof.cl_ulong; //Or 200*Sizeof.cl_uchar

    //work per work element
    private int[] workPerWorkElementArray = new int[1];

    //payload
    private int[] payloadArray = new int[1];

    //winning block
    private int[] winningBlockPayloadArray = {0};
    private int[] winningBlockPayloadOutput = new int[1];

    //shares
    private int sharesSpace = 0;
    private int[] sharesSpaceArray = null;
    private int sharesSpaceSize = 0;
    private int[] sharePayloadsOutput = null;

    private volatile long kernelWaitTime = -1;
    private long pollingTime = Long.MAX_VALUE;
    private BaseValueWatchable<Long> kernelTime = new BaseValueWatchable<>(-1L);
    private static final int REPOLL_CHANCE_DENOMINATOR = 1000;
    private static final int HEAT_TARGET = 50;
    private int heat = 0;
    private KernelType kernel;

    public JOCLSHA3MiningOp(JOCLContextAndCommandQueue _oclContext, DeviceInfo _dInfo, byte[] _blockDifficulty, byte[] _shareDifficulty, int _threads, KernelType _kernel, boolean _greedy) throws Exception
    {
        oclContext = _oclContext;
        blockDifficulty = _blockDifficulty;
        shareDifficulty = _shareDifficulty;
        this.threads = _threads;

        String kernelCompileOptions = (oclContext.getDInfo().isNvidia()) ? "-cl-nv-maxrregcount=256" : "";

        greedy = _greedy;

        String[] kernelSourcePaths;

        kernel = _kernel;

        if (greedy)
        {
            kernelSourcePaths = new String[]{KERNEL_GREEDY_PATH, KERNEL_MAKE_AS_OCL_SOURCE_PATH, KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH, kernel.getSource(), KERNEL_MINER_SOURCE_PATH};
        } else
        {
            kernelSourcePaths = new String[]{KERNEL_MAKE_AS_OCL_SOURCE_PATH, KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH, kernel.getSource(), KERNEL_MINER_SOURCE_PATH};
        }

        kInfo = JOCLKernelCompiler.compileSourceToKernel(oclContext, KERNEL_NAME, kernelSourcePaths, kernelCompileOptions, _blockDifficulty, _shareDifficulty, _threads);

        dInfo = _dInfo;

        //shares
        sharesSpace = kInfo.getSharesSpace();
        sharesSpaceArray = new int[sharesSpace];
        sharesSpaceSize = sharesSpace * Sizeof.cl_uint;
        sharePayloadsOutput = new int[sharesSpace];
    }

    @Override
    public GPUOutput mine(int _threads, byte[] _sponge, int _payload, int _computeUnits) throws Exception
    {
        long kernelMaxWorkGroupSize = kInfo.getKernelMaxWorkGroupSize();

        long[] localWork;
        long[] globalWork;

        int workPerWorkElement = 0;

        if (greedy)
        {
            localWork = new long[]{kernelMaxWorkGroupSize, 1, 1};

            long numWorkElements = _computeUnits * kernelMaxWorkGroupSize;

            workPerWorkElement = (int) Math.ceil((double) _threads / numWorkElements);

            if (workPerWorkElement == 0)
            {
                workPerWorkElement = 1;
            }

            globalWork = new long[]{numWorkElements, 1, 1};
        } else
        {
            globalWork = new long[]{_threads, 1, 1};

            localWork = null;

            if (_threads % kernelMaxWorkGroupSize == 0)
            {
                localWork = new long[]{kernelMaxWorkGroupSize, 1, 1};
            }
        }

        //Sponge allocations
        cl_mem spongeMemory = clCreateBuffer(oclContext.getContext(), CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, SPONGE_SIZE, Pointer.to(_sponge), null);

        //Work Per Work Element allocation
        workPerWorkElementArray[0] = workPerWorkElement;

        //Payload allocation
        payloadArray[0] = _payload;

        //Winner job allocation
        winningBlockPayloadArray[0] = 0;
        cl_mem winningBlockPayloadMemory = clCreateBuffer(oclContext.getContext(), CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR, Sizeof.cl_int, Pointer.to(winningBlockPayloadArray), null);

        //Winning Share allocation


        cl_mem sharesSpaceMemory = clCreateBuffer(oclContext.getContext(), CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, sharesSpaceSize, Pointer.to(sharesSpaceArray), null);

        //Sponge arguments
        clSetKernelArg(kInfo.getOCLKernel(), 0, Sizeof.cl_mem, Pointer.to(spongeMemory));

        //Work Per Work Element argument
        clSetKernelArg(kInfo.getOCLKernel(), 1, Sizeof.cl_int, Pointer.to(workPerWorkElementArray));

        //Payload argument
        clSetKernelArg(kInfo.getOCLKernel(), 2, Sizeof.cl_int, Pointer.to(payloadArray));

        //Winning block payload argument
        clSetKernelArg(kInfo.getOCLKernel(), 3, Sizeof.cl_mem, Pointer.to(winningBlockPayloadMemory));

        //sharesSpace argument
        clSetKernelArg(kInfo.getOCLKernel(), 4, Sizeof.cl_mem, Pointer.to(sharesSpaceMemory));

        cl_event hashingEvent = new cl_event();
        cl_event[] hashEventArray = {hashingEvent};

        clEnqueueNDRangeKernel(oclContext.getCommandQueue(), kInfo.getOCLKernel(), 1, null, globalWork, localWork, 0, null, hashingEvent);
        clFlush(oclContext.getCommandQueue());

        long startTime = System.currentTimeMillis();

        //note to self (Bryan) this causes no discernable difference in performance, but nets us some extra CPU time somehow
        if (kernelWaitTime != -1)
        {
            Synchron.idle((int) kernelWaitTime, 0);
        }

        clWaitForEvents(1, hashEventArray);

        kernelTime.set(System.currentTimeMillis() - startTime);


        //Reads the winning job output and tells this thread to wait until it's done.
        cl_event readingEvent1 = new cl_event();

        clEnqueueReadBuffer(oclContext.getCommandQueue(), winningBlockPayloadMemory, CL_TRUE, 0, Sizeof.cl_uint, Pointer.to(winningBlockPayloadOutput), 0, null, readingEvent1);

        clWaitForEvents(1, new cl_event[]{readingEvent1});


        cl_event readingEvent2 = new cl_event();

        clEnqueueReadBuffer(oclContext.getCommandQueue(), sharesSpaceMemory, CL_TRUE, 0, sharesSpaceSize, Pointer.to(sharePayloadsOutput), 0, null, readingEvent2);

        clWaitForEvents(1, new cl_event[]{readingEvent2});

        //Release the cl_mem buffers.
        clReleaseMemObject(spongeMemory);
        clReleaseMemObject(winningBlockPayloadMemory);
        clReleaseMemObject(sharesSpaceMemory);

        //Releases the cl_event events.
        clReleaseEvent(hashingEvent);
        clReleaseEvent(readingEvent1);
        clReleaseEvent(readingEvent2);

        if(dInfo.isNvidia())
        {
            if (kernelWaitTime == -1)
            {
                if (heat == HEAT_TARGET)
                {
                    kernelWaitTime = pollingTime;
                } else
                {
                    heat++;


                    long temp;

                    if(greedy)
                    {
                        temp = kernelTime.get();
                    }else
                    {
                        temp = kernelTime.get() - 10;
                    }

                    if (temp < pollingTime && temp != 0)
                    {
                        pollingTime = temp;
                    }

                }
            } else
            {
                if (new Random().nextInt(REPOLL_CHANCE_DENOMINATOR) == 0)
                {
                    //Logger logger = GPULogging.getLogger();
                    //if(logger != null)
                    {
                        //logger.debug("Retiming " + dInfo.getDeviceName() + ". Expect higher CPU usage for a short time. Wait time was " + kernelWaitTime);
                    }

                    kernelWaitTime = -1;
                    pollingTime = Long.MAX_VALUE;
                    heat = 0;
                }
            }
        }

        return new GPUOutput(winningBlockPayloadOutput[0], sharePayloadsOutput);
    }

    public BaseValueWatchable<Long> getKernelTime()
    {
        return kernelTime;
    }

    public void resetHeat()
    {
        pollingTime = Long.MAX_VALUE;
        kernelWaitTime = -1;
        heat = 0;
    }

    public KernelInfo getKInfo()
    {
        return kInfo;
    }

    //Call this before sending the mining operation object to the GC.
    public void shutdown() throws Exception
    {
        cl_kernel kernel = kInfo.getOCLKernel();

        if (kernel != null)
        {
            clReleaseKernel(kernel);
        }
    }

    @Override
    public void setGreedy(boolean val) {

        String kernelCompileOptions = (oclContext.getDInfo().isNvidia()) ? "-cl-nv-maxrregcount=256" : "";
        String[] kernelSourcePaths;

        if (val)
        {
            kernelSourcePaths = new String[]{KERNEL_GREEDY_PATH, KERNEL_MAKE_AS_OCL_SOURCE_PATH, KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH, kernel.getSource(), KERNEL_MINER_SOURCE_PATH};
        } else
        {
            kernelSourcePaths = new String[]{KERNEL_MAKE_AS_OCL_SOURCE_PATH, KERNEL_KECCAK_UNIVERSAL_SOURCE_PATH, kernel.getSource(), KERNEL_MINER_SOURCE_PATH};
        }

        try {
            kInfo = JOCLKernelCompiler.compileSourceToKernel(oclContext, KERNEL_NAME, kernelSourcePaths, kernelCompileOptions, blockDifficulty, shareDifficulty, threads);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        greedy = val;
    }
}
