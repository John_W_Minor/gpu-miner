package gpuminer.JOCL.context;

import gpuminer.miner.infocontainers.DeviceInfo;
import gpuminer.JOCL.infocontainers.JOCLPlatformInfo;
import org.jocl.CLException;
import org.jocl.cl_device_id;
import org.jocl.cl_platform_id;

import static org.jocl.CL.*;

/**
 * Created by Queue on 10/12/2017.
 */
public class JOCLDevices
{
    private JOCLContextAndCommandQueue[] oclContexts = null;

    private cl_device_id[] devices = null;

    public JOCLDevices(JOCLPlatformInfo _pInfo) throws Exception
    {
        cl_platform_id platform = _pInfo.getPlatform();

        int devicesCountArray[] = new int[1];
        clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, null, devicesCountArray);
        int devicesCount = devicesCountArray[0];

        devices = new cl_device_id[devicesCount];
        clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, devicesCount, devices, null);

        oclContexts = new JOCLContextAndCommandQueue[devices.length];

        for (int i = 0; i < devices.length; i++)
        {
            DeviceInfo dInfo = new DeviceInfo(devices[i]);
            oclContexts[i] = new JOCLContextAndCommandQueue(_pInfo, dInfo);
        }
    }

    public JOCLContextAndCommandQueue[] getOclContexts()
    {
        if (oclContexts.length != 0)
        {
            return oclContexts.clone();
        }
        return null;
    }

    public void shutdown() throws Exception
    {
        for (int i = 0; i < devices.length; i++)
        {
            clReleaseDevice(devices[i]);
        }
    }
}
