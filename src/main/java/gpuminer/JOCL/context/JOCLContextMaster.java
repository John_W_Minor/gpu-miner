package gpuminer.JOCL.context;

import gpuminer.GPULogging;
import gpuminer.JOCL.infocontainers.JOCLPlatformInfo;
import logging.IAmpexLogger;
import org.jocl.*;

import java.util.ArrayList;

import static org.jocl.CL.*;

/**
 * Created by Queue on 10/1/2017.
 */
public class JOCLContextMaster
{
    private JOCLDevices[] deviceBuckets = null;

    private ArrayList<JOCLContextAndCommandQueue> oclContexts = new ArrayList<>();

    public JOCLContextMaster() throws Exception
    {
        CL.setExceptionsEnabled(true);

        int platformsCountArray[] = new int[1];
        clGetPlatformIDs(0, null, platformsCountArray);
        int platformsCount = platformsCountArray[0];

        cl_platform_id platforms[] = new cl_platform_id[platformsCount];
        clGetPlatformIDs(platforms.length, platforms, null);

        JOCLPlatformInfo[] pInfos = new JOCLPlatformInfo[platforms.length];
        deviceBuckets = new JOCLDevices[platforms.length];

        for (int i = 0; i < platforms.length; i++)
        {
            pInfos[i] = new JOCLPlatformInfo(platforms[i]);
            deviceBuckets[i] = new JOCLDevices(pInfos[i]);

            JOCLContextAndCommandQueue[] deviceOCLContext = deviceBuckets[i].getOclContexts();

            for (int j = 0; j < deviceOCLContext.length; j++)
            {
                if (deviceOCLContext[j].getDInfo().isAvailable() && deviceOCLContext[j].isValidDevice())
                {
                    oclContexts.add(deviceOCLContext[j]);
                }
            }
        }
        IAmpexLogger logger = GPULogging.getLogger();
        if(logger != null)
        {
            System.out.println();
            logger.info("OCL Contexts generated.");
        }
    }

    public ArrayList<JOCLContextAndCommandQueue> getOCLContexts()
    {
        if (oclContexts != null)
        {
            ArrayList<JOCLContextAndCommandQueue> outgoing = new ArrayList<>();
            outgoing.addAll(oclContexts);
            return outgoing;
        }
        return null;
    }

    public void shutdown() throws Exception
    {
        if (oclContexts != null)
        {
            for (JOCLContextAndCommandQueue oclContext : oclContexts)
            {
                oclContext.shutdown();
            }

            oclContexts = null;
        }

        if (deviceBuckets != null)
        {
            for (JOCLDevices deviceBucket : deviceBuckets)
            {
                deviceBucket.shutdown();
            }

            deviceBuckets = null;
        }
    }
}
