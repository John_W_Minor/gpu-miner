package gpuminer.JOCL.context;

import gpuminer.GPULogging;
import gpuminer.miner.constants.Constants;
import gpuminer.miner.infocontainers.DeviceInfo;
import gpuminer.JOCL.infocontainers.JOCLPlatformInfo;
import logging.IAmpexLogger;
import org.jocl.*;

import static org.jocl.CL.*;

/**
 * Created by Queue on 10/12/2017.
 */
public class JOCLContextAndCommandQueue
{
    private JOCLPlatformInfo pInfo = null;

    private cl_platform_id platform = null;

    private DeviceInfo dInfo = null;

    private cl_device_id device = null;

    private cl_context context = null;

    private cl_command_queue commandQueue = null;

    private boolean validDevice = false;

    public JOCLContextAndCommandQueue(JOCLPlatformInfo _pInfo, DeviceInfo _dInfo) throws Exception
    {
        pInfo = _pInfo;

        platform = pInfo.getPlatform();

        dInfo = _dInfo;

        device = dInfo.getOCLDevice();

        boolean isSupported = true;//dInfo.isAmd() || dInfo.isNvidia();

        if (dInfo.isAvailable() && dInfo.getDeviceType().equals(Constants.TYPE_GPU) && isSupported)
        {
            cl_context_properties contextProperties = new cl_context_properties();
            contextProperties.addProperty(CL_CONTEXT_PLATFORM, platform);

            context = clCreateContext(contextProperties, 1, new cl_device_id[]{device}, null, null, null);

            IAmpexLogger logger = GPULogging.getLogger();

            try
            {
                commandQueue = clCreateCommandQueueWithProperties(context, device, new cl_queue_properties(), null);
                validDevice=true;
            } catch (Exception e)
            {
                if(logger != null)
                {
                    logger.info("Reverting to OpenCL 1.0 / 1.1 command queue for " + dInfo.getDeviceName());
                }
            }

            if(!validDevice)
            {
                try
                {
                    commandQueue = clCreateCommandQueue(context, device, 0, null);
                    if(logger != null)
                    {
                        logger.info("Reverting complete for " + dInfo.getDeviceName() + ".");
                    }
                    validDevice = true;
                } catch (Exception e)
                {
                    clReleaseContext(context);
                    if(logger != null)
                    {
                        logger.warn("Reverting failed. " + dInfo.getDeviceName() + " does not seem to be a valid device.");
                    }
                }
            }
        }
    }

    public JOCLPlatformInfo getPInfo()
    {
        return pInfo;
    }

    public cl_platform_id getPlatform()
    {
        return platform;
    }

    public DeviceInfo getDInfo()
    {
        return dInfo;
    }

    public cl_device_id getDevice()
    {
        return device;
    }

    public cl_context getContext()
    {
        return context;
    }

    public cl_command_queue getCommandQueue()
    {
        return commandQueue;
    }

    public boolean isValidDevice()
    {
        return validDevice;
    }

    public void shutdown() throws Exception
    {
        if(commandQueue!=null)
        {
            clReleaseCommandQueue(commandQueue);
        }

        commandQueue=null;

        if (context!=null)
        {
            clReleaseContext(context);
        }

        context=null;
    }

    //@Deprecated
    //public static boolean noIntel=false;

    @Deprecated
    public static void setWorkaround(boolean workaround)
    {
    }
}
