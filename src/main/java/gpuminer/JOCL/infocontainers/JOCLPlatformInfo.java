package gpuminer.JOCL.infocontainers;

import gpuminer.miner.constants.Constants;
import org.jocl.Pointer;
import org.jocl.Sizeof;
import org.jocl.cl_platform_id;

import static org.jocl.CL.CL_PLATFORM_VERSION;
import static org.jocl.CL.clGetPlatformInfo;

/**
 * Created by Queue on 10/11/2017.
 */
public class JOCLPlatformInfo
{
    private cl_platform_id platform = null;

    private String openCLVersion = null;

    public JOCLPlatformInfo(cl_platform_id _platform) throws Exception
    {
        platform=_platform;

        byte[] openCLVersionArray = new byte[100];
        clGetPlatformInfo(platform, CL_PLATFORM_VERSION, 100* Sizeof.cl_char , Pointer.to(openCLVersionArray) ,null);
        openCLVersion = new String(openCLVersionArray, Constants.UTF8).trim();

        int a=0;
    }

    public cl_platform_id getPlatform()
    {
        return platform;
    }

    public String getOpenCLVersion()
    {
        return openCLVersion;
    }
}
